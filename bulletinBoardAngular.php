<?php include 'loginRequired.php'; ?>
<?php $titleValue = "Home Owners Association: Bulletin Board"; ?>
<?php $activePage = "bulletinBoard"; ?>
<html lang="en" ng-app="myApp">
<?php include 'homeOwnersAssociationHeader.php'; ?>

<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

<div class = "main" ng-controller = "MyController">
    <div class="search">
        <label>Search: </label>
        <input ng-model="query" placeholder="Search Bulletin Board" autofocus>

           <div class="bulletinBoardSearch" ng-repeat="variableItem in bulletin | filter: query">
               <!-- <img ng-src="images/{{variableItem.shortname}}_tn.jpg" alt="Photo of {{variableItem}}"> -->
               <div class="info">



      <div id="anotherPost" + {{variableItem.homeOwnersAssociationPostNumber}} + "\" " class="postWrapper"><br />
      <img src="Icons/redCancelCircle.svg" title="Hide Post (Does Not Delete.  You Can Recover the Entry by Refreshing the Page)" alt="Hide Post (Does Not Delete.  You Can Recover the Entry by Refreshing the Page)" width="20px" class = "cancelButton" onclick="removePost('{{variableItem.homeOwnersAssociationPostNumber}}');"><br />
            
      <div class="whiteWrapper">
      <h1>{{variableItem.subject}}</h1>
      </div>

      <div class="whiteWrapper">
      {{variableItem.content}}
      </div>
      Submitted by {{variableItem.userName}} <br />
      Submission Date: {{variableItem.submission_date}}<br />
      Submission Time: {{variableItem.submission_time}}<br />
      <input type="button" name="button" id="button" value="Update" onclick="updatePostDirect('" + {{variableItem.homeOwnersAssociationPostNumber}} + "');">
      <input type="button" name="button" id="button" value="Delete" onclick="deletePostDirect('" + {{variableItem.homeOwnersAssociationPostNumber}} + "');">
      </div>

    </div>
</div>
<!-- Create script to delete a Post with a confirmation pop up. -->


<?php

if(isset($_GET['postDeleteConfimationCheck'])){
    
    if( $_GET['postDeleteConfimationCheck'] == 1 ){
            
            include 'dbConnect.php';
            $sql = "SELECT * FROM homeOwnersAssociationPostsTable WHERE homeOwnersAssociationPost_id = " . $_GET['recordId'];   //build the SQL query
                //Note the WHERE clause allows us to select ONLY the desired record
    
            //3.  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
          $result = mysqli_query($link,$sql);
            

          if(!$result )             //Make sure the Query ran correctly and created result
          {
            echo "<h1 style='color:red'>Houston, We have a problem!</h1>";  //Problems were encountered.
            echo mysqi_error($link);    //Display error message information
          }


            echo "<p align=\"center\"><div class=\"whiteWrapper\"><h1>Are you sure you want to delete the following record?</h1></p>"; 
            while($row = mysqli_fetch_array($result))   //Turn each row of the result into an associative array 
            {
                //Display the selected post.
                echo "<div id=\"deletingPost\" class=\"postWrapper\"><br />"; 
                echo "<div class=\"whiteWrapper\">";
                echo $row['content'];
                echo "</div>";     
                echo "Submitted by " . $row['userName'] . "<br />";
                echo "<br />Submission Date: " . $row['submission_date'];
                echo "<br />Submission Time: " . $row['submission_time'] . "<br />";  
                echo "<br /><br />";
                echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Yes\" onclick=\"deletePostYes(" . $row['homeOwnersAssociationPost_id'] . ");\" />";
                echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"No\" onclick=\"deletePostNo(" . $row['homeOwnersAssociationPost_id'] . ");\" />";
                echo "</div>";
                echo "</div>";
          }
            
                echo "<br /><br />";


        }
    elseif ($_GET['postDeleteConfimationCheck'] == 2 ){
    
        if(isset($_GET['recordId'])){
      
              $deleteRecId = $_GET['recordId'];   //Pull the event_id from the GET parameter
            
              include 'dbConnect.php';    //connects to the database
           
              $sqlSelect = "SELECT * FROM homeOwnersAssociationPostsTable";   //build the SQL query
                        //Note the WHERE clause allows us to select ONLY the desired record
            
                //  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
              $result = mysqli_query($link,$sqlSelect);   //run the Query and store the result in $result
            
              if(!$result )             //Make sure the Query ran correctly and created result
              {
                echo "<h1 style='color:red'>Something went wrong.</h1>";  //Problems were encountered.
                echo mysqi_error($link);    //Display error message information
              }
            
                include 'dbConnect.php';    //connects to the database
                echo "<div class=\"postWrapper\">";
                echo "<h2>Record number: " . $_GET['recordId'] . " has been deleted.</h2>"; //Display a message verifying the record to be deleted.  This could be turned into a second confirmation
                
                $sql = "DELETE FROM homeOwnersAssociationPostsTable WHERE homeOwnersAssociationPost_id = $deleteRecId";
                  //echo "<p>The SQL Command: $sql </p>";     //testing
              
                if (mysqli_query($link,$sql) )          //process the query
                {     

                echo "<h1>Your record has been successfully deleted.</h1>";
                echo "<p><a href='bulletinBoard.php'>Return to Bulletin Board</a></p>";  
                echo "</div>";
                }
                else
                {
                  echo "<h1>You have encountered a problem with your delete.</h1>";
                  echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
                }
            }
    
        }
    else {

        //append the table
        // include 'displayTable.php';
    
    }
}
else {

    //append the table
    // include 'displayTable.php';

}


?>


<?php

  //2.  Create an SQL SELECT command that will pull all the posts from posts table.
  $sql = "SELECT * FROM homeOwnersAssociationPostsTable ORDER BY submission_date DESC, submission_time DESC";   //build the SQL query
            //Note the WHERE clause allows us to select ONLY the desired record

    //3.  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
  $result = mysqli_query($link,$sql);   //run the Query and store the result in $result

  if(!$result )             //Make sure the Query ran correctly and created result
  {
    echo "<h1 style='color:red'>There is a problem.</h1>";  //Problems were encountered.
    echo mysqi_error($link);    //Display error message information
  }
?>

<div class="centerClass">
<div class="buttonWrapper">
<br />
<!-- Create script to compromise spacing based upon the presence of Flash -->

<script>

//add an swfObject class to html in order to check for the existence of Flash Player
$('html').addClass(typeof swfobject !== 'undefined' && swfobject.getFlashPlayerVersion().major !== 0 ? 'flash' : 'no-flash');
//if it has Flash Player do nothing
if( swfobject.hasFlashPlayerVersion("8.0") ) { 

    document.write(" ");

}

else {
//if it doesn't have Flash Player add an additional break to compensate for spacing.
    document.write("<br />");

}

</script>

<h1><input type="button" class = "blueButton" name="addNewPost" id="addNewPost" value="Add New Post" onclick="newPostDirect();"></h1>
</div>
<?php

$postNumber = 0;


//4.  Use a PHP loop to process each row in the result.
  echo "<script>var postsPulledFromDatabase = " . mysqli_num_rows($result) . "; </script>";

  while($row = mysqli_fetch_array($result))   //Turn each row of the result into an associative array 
    {

      global $postNumber;

      echo "<div id=\"anotherPost" . $postNumber . "\" class=\"postWrapper\"><br />"; 
      echo "<img src=\"Icons/redCancelCircle.svg\" title=\"Hide Post (Does Not Delete.  You Can Recover the Entry by Refreshing the Page)\" alt=\"Hide Post (Does Not Delete.  You Can Recover the Entry by Refreshing the Page)\" width=\"20px\" class = \"cancelButton\" onclick=\"removePost('" . $postNumber . "');\"><br />";
      $subjectValueRemoveSpaces = str_replace("-"," ",$row['subject']); //convert - to ""
      //echo "<script>alert('" . $nameValueRemoveSpaces . "');</script>";
      
      echo "<div class=\"whiteWrapper\">";
      echo "<h1>" . $subjectValueRemoveSpaces . "</h1>";
      echo "</div>";

      ?>

      <?php

              echo "<div class=\"whiteWrapper\">";
              echo $row['content'];
              echo "</div>";
        
              echo "Submitted by " . $row['userName'] . "<br />";
              echo "<br />Submission Date: " . $row['submission_date'];
              echo "<br />Submission Time: " . $row['submission_time'] . "<br />";  

              if($_SESSION['userID'] != $row['userID']){

                if($_SESSION['adminPrivileges'] != "1"){
                  //not valid user
                }

                else {

                      echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Update\" onclick=\"updatePostDirect('" . $row['homeOwnersAssociationPost_id'] . "');\">";
                      echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Delete\" onclick=\"deletePostDirect('" . $row['homeOwnersAssociationPost_id'] . "');\">";

                }
              }
              else {

                      echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Update\" onclick=\"updatePostDirect('" . $row['homeOwnersAssociationPost_id'] . "');\">";
                      echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Delete\" onclick=\"deletePostDirect('" . $row['homeOwnersAssociationPost_id'] . "');\">";


              }

              echo "</div>";


              $postNumber += 1;

    }
              
?>

</div>
<?php include 'homeOwnersAssociationFooter.php'; ?>
