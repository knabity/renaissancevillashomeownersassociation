<?php include 'loginRequired.php'; ?>
<?php $titleValue = "Delete By Laws"; ?>
<?php include 'homeOwnersAssociationHeader.php'; ?>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<?php

	$deleteRecId = $_GET['recordId'];		//Pull the event_id from the GET parameter

	include 'dbConnect.php';		//connects to the database

	$sqlSelect = "SELECT * FROM homeOwnersAssociationByLawsTable";		//build the SQL query
						//Note the WHERE clause allows us to select ONLY the desired record

    //  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
	$result = mysqli_query($link,$sqlSelect);		//run the Query and store the result in $result

	if(!$result )							//Make sure the Query ran correctly and created result
	{
		echo "<h1 style='color:red'>Something went wrong.</h1>";	//Problems were encountered.
		echo mysqi_error($link);		//Display error message information
	}

?>


<div class="beigeBodyWrapper80">
   
    <?php
            
        include 'dbConnect.php';    //connects to the database

        $sqlUserComparison = "SELECT * FROM homeOwnersAssociationByLawsTable WHERE homeOwnersAssociationByLaws_id = $deleteRecId";   //build the SQL query
        $result = mysqli_query($link,$sqlUserComparison);   //run the Query and store the result in $result
        
          if(!$result )             //Make sure the Query ran correctly and created result
          {
            echo "<h1 style='color:red'>There is a problem.</h1>";  //Problems were encountered.
            echo mysqi_error($link);    //Display error message information
          }
        while($row = mysqli_fetch_array($result))   //Turn each row of the result into an associative array 
            {
        
            //if it is not being deleted by the person that wrote it or the administrator prompt an error.
              //check if it was not written by the person deleting it.
            if($_SESSION['userID'] != $row['userID']){
                //check if it was not written by the administrator.
                if($_SESSION['adminPrivileges'] != "1"){
                //echo you don't have permissions to delete it.
                echo "<h1 class=\"red\">You do not have permission to delete this post since you did not write it.</h1>";
                
                }

                else {
                    //this is the code that activates when the administrator is logged in. It will delete the post.
                    $sql = "DELETE FROM homeOwnersAssociationByLawsTable WHERE homeOwnersAssociationByLaws_id = $deleteRecId";
                      //echo "<p>The SQL Command: $sql </p>";     //testing
                      
                    if (mysqli_query($link,$sql) )          //process the query
                    {
                      echo "<h1>By law Number: " . $_GET['recordId'] . " has been successfully deleted.</h1>";
                    }
                    else
                    {
                      echo "<h1>You have encountered a problem with your delete.</h1>";
                      echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
                    }
                    
                    mysqli_close($link);    //close the database connection and free up server resources
                }

              }
        
            //this code activates when the user is the person who wrote the post.  It will delete the post.
            else {
            
                $sql = "DELETE FROM homeOwnersAssociationByLawsTable WHERE homeOwnersAssociationByLaws_id = $deleteRecId";
	                //echo "<p>The SQL Command: $sql </p>";     //testing
                	
                if (mysqli_query($link,$sql) )					//process the query
                {
	                echo "<h1>Record Number: " . $_GET['recordId'] . " has been successfully deleted.</h1>";
                }
                else
                {
	                echo "<h1>You have encountered a problem with your delete.</h1>";
	                echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
                }
                
                mysqli_close($link);		//close the database connection and free up server resources
                
                }
            }
    ?>

</div>
<div class="titleWrapper">
    
    <input type="button" name="addNewByLaw" id="addNewByLaw" value="Add New By Law" onclick="newByLawDirect();" />
    <input type="button" name="viewArticleAndByLaws" id="viewArticleAndByLaws" value="View By Laws" onclick="viewArticleAndByLawsDirect();" />

</div>

    <?php
   
      include 'dbConnect.php';
      //2.  Create an SQL SELECT command that will pull all the recipes from your recipes table.
      $sql = "SELECT * FROM homeOwnersAssociationByLawsTable";   //build the SQL query
                //Note the WHERE clause allows us to select ONLY the desired record
    
        //3.  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
      $result = mysqli_query($link,$sql);   //run the Query and store the result in $result
    
      if(!$result )             //Make sure the Query ran correctly and created result
      {
        echo "<h1 style='color:red'>There is a problem.</h1>";  //Problems were encountered.
        echo mysqi_error($link);    //Display error message information
      }

    $postNumber = 0;
   
    //4.  Use a PHP loop to process each row in the result.
      echo "<script>var postsPulledFromDatabase = " . mysqli_num_rows($result) . "; </script>";
    
    ?>

    <?php include 'homeOwnersAssociationFooter.php'; ?>
