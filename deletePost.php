<?php include 'loginRequired.php'; ?>
<?php $titleValue = "Delete Post"; ?>
<?php include 'homeOwnersAssociationHeader.php'; ?>

<?php

	$deleteRecId = $_GET['recordId'];		//Pull the event_id from the GET parameter

	include 'dbConnect.php';		//connects to the database

	$sqlSelect = "SELECT * FROM homeOwnersAssociationPostsTable";		//build the SQL query
						//Note the WHERE clause allows us to select ONLY the desired record

    //  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
	$result = mysqli_query($link,$sqlSelect);		//run the Query and store the result in $result

	if(!$result )							//Make sure the Query ran correctly and created result
	{
		echo "<h1 style='color:red'>Something went wrong.</h1>";	//Problems were encountered.
		echo mysqi_error($link);		//Display error message information
	}

?>


<div class="beigeBodyWrapper80">
   
    <?php
    
        include 'dbConnect.php';		//connects to the database
        
        $sqlUserComparison = "SELECT * FROM homeOwnersAssociationPostsTable WHERE homeOwnersAssociationPost_id = $deleteRecId";   //build the SQL query
        $result = mysqli_query($link,$sqlUserComparison);   //run the Query and store the result in $result
        
          if(!$result )             //Make sure the Query ran correctly and created result
          {
            echo "<h1 style='color:red'>There is a problem.</h1>";  //Problems were encountered.
            echo mysqi_error($link);    //Display error message information
          }
        while($row = mysqli_fetch_array($result))   //Turn each row of the result into an associative array 
            {
        
            if($_SESSION['userName'] != $row['submitted_By']){
            
                echo "<h1 class=\"red\">You do not have permission to delete this post since you did not write it.</h1>";
            
            }
        
            else {
            
                $sql = "DELETE FROM homeOwnersAssociationPostsTable WHERE homeOwnersAssociationPost_id = $deleteRecId";
	                //echo "<p>The SQL Command: $sql </p>";     //testing
                	
                if (mysqli_query($link,$sql) )					//process the query
                {
	                echo "<h1>Record Number: " . $_GET['recordId'] . " has been successfully deleted.</h1>";
                }
                else
                {
	                echo "<h1>You have encountered a problem with your delete.</h1>";
	                echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
                }
                
                mysqli_close($link);		//close the database connection and free up server resources
                }
            }
    ?>

</div>
<div class="titleWrapper">
    
    <input type="button" name="addNewPost" id="addNewPost" value="Add New Post" onclick="addNewPost();" />
</div>

    <?php
   
      include 'dbConnect.php';
      //2.  Create an SQL SELECT command that will pull all the recipes from your recipes table.
      $sql = "SELECT * FROM homeOwnersAssociationPostsTable";   //build the SQL query
                //Note the WHERE clause allows us to select ONLY the desired record
    
        //3.  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
      $result = mysqli_query($link,$sql);   //run the Query and store the result in $result
    
      if(!$result )             //Make sure the Query ran correctly and created result
      {
        echo "<h1 style='color:red'>There is a problem.</h1>";  //Problems were encountered.
        echo mysqi_error($link);    //Display error message information
      }

    $postNumber = 0;
   
    //4.  Use a PHP loop to process each row in the result.
      echo "<script>var postsPulledFromDatabase = " . mysqli_num_rows($result) . "; </script>";
    
      while($row = mysqli_fetch_array($result))   //Turn each row of the result into an associative array 
        {
    
          global $postNumber;
    
          echo "<div id=\"anotherPost" . $postNumber . "\" class=\"beigeWrapper\"><br />"; 
          echo "<img src=\"Icons/redCancelCircle.svg\" width=\"20px\" class = \"cancelButton\" onclick=\"removePost('" . $postNumber . "');\"><br />";
          echo "<h1>" . $row['subject'] . "</h1>";
    
                }
    
          echo "</div>";
          echo "<div class=\"whiteWrapper\">";
          echo $row['content'];
          echo "</div>";
    
          //create for loop to output numbers
          for ($i = 1; $i <= 100; $i++) {
              echo "<option value=\"" . $i . "\">" . $i . "</option>";
          }
    
          echo "</select><br />";
    
          echo "Submitted by " . $row['nameOfPoster'];
          echo "<br />Submission Date: " . $row['submission_date'];
          echo "<br />Submission Time: " . $row['submission_time'];  
          echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Update\" onclick=\"updateDirect('" . $row['homeOwnersAssociationPost_id'] . "');\">";
          echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Delete\" onclick=\"deleteDirect('" . $row['homeOwnersAssociationPost_id'] . "');\">";
          echo "</div>";
          
          $postNumber += 1;
    
        }
    
    
    ?>

    <?php include 'homeOwnersAssociationFooter.php'; ?>
