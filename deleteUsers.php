<?php include 'loginRequired.php'; ?>
<?php $titleValue = "Delete Users"; ?>
<?php include 'homeOwnersAssociationHeader.php'; ?>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<?php

	$deleteRecId = $_GET['recordId'];		//Pull the event_id from the GET parameter

	include 'dbConnect.php';		//connects to the database

	$sqlSelect = "SELECT * FROM homeOwnersAssociationOwnersTable";		//build the SQL query
						//Note the WHERE clause allows us to select ONLY the desired record

    //  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
	$result = mysqli_query($link,$sqlSelect);		//run the Query and store the result in $result

	if(!$result )							//Make sure the Query ran correctly and created result
	{
		echo "<h1 style='color:red'>Something went wrong.</h1>";	//Problems were encountered.
		echo mysqi_error($link);		//Display error message information
	}

?>

</head>

<body>

<div class="beigeBodyWrapper">
<h1>Home Owners Association: Delete User</h1>
</div>
<div class="beigeBodyWrapper">
<?php

include 'dbConnect.php';		//connects to the database

if($_SESSION['userName'] == "administrator"){

    echo "<h2>Record Number : " . $_GET['recordId'] . " has been deleted.</h2>";	//Display a message verifying the record to be deleted.  This could be turned into a second confirmation
    
    $sql = "DELETE FROM homeOwnersAssociationOwnersTable WHERE homeOwnersAssociationOwner_id = $deleteRecId";
	    //echo "<p>The SQL Command: $sql </p>";     //testing
    	
    if (mysqli_query($link,$sql) )					//process the query
    {
	    echo "<h1>Your record has been successfully deleted.</h1>";
	    echo "<p><a href='owners.php'>View Owners</a></p>";	
    }
    else
    {
	    echo "<h1>You have encountered a problem with your delete.</h1>";
	    echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
    }
}
else {

        echo "<h1 class=\"red\">You do not have permission to delete users.  Please contact the website administrator.</h1>";

}
mysqli_close($link);		//close the database connection and free up server resources
?>

<script>

    function deleteUserDirect(recordId){

        document.getElementbyId
        window.location.replace("deleteUsers.php?recordId=" + recordId);
        
    }

    function updateUserDirect(recordId){

        document.getElementbyId
        window.location.replace("updateUsers.php?recordId=" + recordId);
        
    }
</script>

<div>


<?php

        if(isset($_SESSION['validUser'])){

            if($_SESSION['validUser'] == "yes" && $_SESSION['adminPrivileges'] == "1")
            
            {
?>


<div>
                <a href="adminCSVFile.csv">Click Here to Download a CSV File</a>

                <table border="1">
                <tr>
                <th>User Name:</th>
                    <th>First Name:</th>
                    <th>Last Name:</th>
                    <th>Email Address:</th>
                    <th>Phone Number:</th>
                    <th>List Phone:</th>
                <th>Password:</th>
                    <th>Admin Privileges:</th>
                <th>Registration Date:</th>
                <!-- <th>Registration Time:</th> -->
                  <th>Update:</th>
                <th>Delete:</th>
              </tr>    


<?php
    
    $rowNumber = 0;
    $ownerArray = array("User Name" . "," . "First Name" . "," . "Last Name" . "," . "Email Address" . "," . "Phone Number" . "," . "Street Address" . "," . "City, State & Zip Code",);

//4.  Use a PHP loop to process each row in the result.
  while($row = mysqli_fetch_array($result))   //Turn each row of the result into an associative array 
    {
    //For each row you found int the table create an HTML table in the response object

//5.  Format each row from the result into an HTML table row.
//6.  Display the final results to the client.
    
                global $rowNumber;
                $newNumber = $rowNumber%2;
                echo "<tr class=\"row" . $newNumber . "\">";               
                echo "<td id=\"" . $row['userName'] . "\">" . $row['userName'] . "</td>";
                echo "<td>" . $row['firstName'] . "</td>";
                echo "<td>" . $row['lastName'] . "</td>";
                echo "<td>" . $row['emailAddress'] . "</td>";
                $rowNumber++;

                //if the owner wants their number listed, list it.  Otherwise do not list it.

                if( $row['listPhone'] == "yes"){

                echo "<td>" . $row['phoneNumber'] . "</td>";

                }

                else {

                echo "<td>••••••••••</td>";

                }

                echo "<td>" . $row['listPhone'] . "</td>";
              echo "<td>" . "••••••••" . "</td>";

              if( $row['adminPrivileges'] == "1" ){

                    echo "<td>Yes</td>";

              }

              else {

                    echo "<td>No</td>";


              }

              echo "<td>" . $row['submission_date'] . "</td>";
              //echo "<td>" . $row['submission_time'] . "</td>";
            echo "<td><input type=\"button\" name=\"button\" id=\"button\" value=\"Update\" onclick=\"updateUserDirect(" . $row['homeOwnersAssociationOwner_id'] . ");\" />";
            echo "<td><input type=\"button\" name=\"button\" id=\"button\" value=\"Delete\" onclick=\"deleteUserDirect(" . $row['homeOwnersAssociationOwner_id'] . ");\" />";
            echo "</tr>";


                //update the Administrator View CSV Document
                global $ownerArray; //call global $ownerArray variable

                //create a new User Array based on the user in this iteration
                $newUserArray = array(

                    $row['userName'] . "," . $row['firstName'] . "," . $row['lastName'] . "," . $row['emailAddress'] . "," . $row['phoneNumber'] . "," . $row['streetAddress'] . "," . $row['cityStateZip'],

                );
                
                //for each value of the $newUserArray, push it into the global $ownerArray
                foreach ($newUserArray as $value){
                    
                    global $ownerArray;

                    array_push($ownerArray, $value);

                }

                //array_push($ownerArray, $newUserArray);
                //open the CSV document with write permissions
                $adminCSVFile = fopen('adminCSVFile.csv', 'w');

                //for each value of the $ownerArray explode it by the commas and write it to the CSV document                
                foreach ($ownerArray as $value)
                {
                
                    fputcsv($adminCSVFile,explode(',',$value),",","\"");

                }

                //close the CSV Document
                fclose($adminCSVFile);

    }  //end of while loop to create table
  echo "</table>";    //Placed this command in the HTML instead of using the echo

    echo "<br /><p><a href=\"userForm.php\" class=\"blueButton\">Add New Owner</a></p>";
}
              else{ echo ""; }
      }
      else{ echo ""; }


?>
	</table>
  </div>
  </div>
  </div>
  <?php include 'homeOwnersAssociationFooter.php'; ?>
