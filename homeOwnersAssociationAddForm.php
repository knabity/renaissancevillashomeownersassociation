<?php include 'loginRequired.php'; ?>
<?php $activePage = "addNewPost"; ?>
<?php $titleValue = "Add Your Post"; ?>
<?php include 'homeOwnersAssociationHeader.php'; ?>
<?php include 'createHomeOwnersAssociationTables.php'; ?>
<link rel="stylesheet" type="text/css" href="homeOwnersAssociationStyleSheet.css">
<style>

.newRecipe p {

    text-align: center;

}

</style>
<!--Retrieve ingredient array from database-->

  <div class="beigeBodyWrapper80">
  <h1>Add New Post</h1>
     
    <?php include 'includedAssets.php'; ?>
<div class="newPost">
    <form id="newPostForm" name="newPostForm" method="post" action="newPostProcess.php"></p>
      <input type="text" id="productImage" name="productImage" class="hidden" maxlength="255" min="1" required size="30" onchange="leftTrimSpecialCharacters('fileName');">
      <p>Recipe Name: <input type="text" id="recipeName" name="recipeName" class="textField" id="recipeName" maxlength="255" min="1" required size="30" onchange="leftTrimSpecialCharacters('recipeName');"></p>
      <div id="recipeNameError" class="red"></div>
      <p>Number of Servings For the Recipe: <input type="text" name="numberOfServings" class="textField" id="numberOfServings" maxlength="255" min="1" required size="30" onchange="leftTrimNumeric('numberOfServings');"></p>
      <div id="recipeNumberOfServingsError" class="red"></div>
      <p>Average Cooking Time (In Minutes): <input type="text" name="cookingTime" class="textField" id="averageCookingTime" maxlength="255" min="1" required size="30" onchange="leftTrimNumeric('averageCookingTime');"></p>
      <div id="averageCookingTimeError" class="red"></div>
      <button type="button"  onclick="addAnotherIngredient();" class="blueButton">Add Another Ingredient</button>
      <div id="addAnotherIngredientDiv"></div>
      
      <input type="text" class="hidden" name="addAnotherIngredientForSubmit" id="addAnotherIngredientForSubmit">
      <input type="text" class="hidden" name="numberOfAddedAnotherIngredient" id="numberOfAddedAnotherIngredient">

<div id="ingredientButtons" class="blueDivider">

      <!--New Ingredients Added:--> 
      <input type="text" class="hidden" name="newIngredients" id="newIngredients">
      <input type="text" class="hidden" name="hiddenSubmittedUser" id="hiddenSubmittedUser">
      <!-- Assign user name to hiddenSubmittedUser -->

</div>

      <div id="recipeInstructionsDiv" class="blueDivider">
      <h2>Enter your recipe instructions:</h2> 
      <p>Subject: <input type="text" name="subject" class="textField" maxlength="255" size="100"></p>
      <p><textarea name="content" class="largeTextArea" id="content" maxlength="2000" form="recipeForm" required onchange="leftTrimSpecialCharacters('recipeInstructions');">Enter text here...</textarea></p>

      <p><input type="submit" name="button" id="button" value="Add Recipe" class="blueButton"/>
      <input type="reset" name="button2" id="button2" value="Clear Form" class="blueButton"/></p>
</div>
</div>

    </form>
  </div>
</div>
<?php include 'homeOwnersAssociationFooter.php'; ?>
