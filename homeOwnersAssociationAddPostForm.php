<?php include 'loginRequired.php'; ?>
<?php $activePage = "addNewPost"; ?>
<?php $titleValue = "Add Your Post"; ?>
<?php include 'homeOwnersAssociationHeader.php'; ?>
<?php include 'createHomeOwnersAssociationTables.php'; ?>
<link rel="stylesheet" type="text/css" href="homeOwnersAssociationStyleSheet.css">
<style>

.newPost p {

    text-align: center;

}

</style>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<form id="addNewPostForm" name="addNewPostForm" method="post" action="postProcess.php">

  <div class="beigeBodyWrapper80">
  <h1>Add New Post</h1>
     
<div class="newPost">
    
</div>

      <div class="whiteWrapper">
      <!-- Create Hidden User ID Fields -->
      <input type="hidden" name="userName" class="hidden" maxlength="255" size="50" value="<?php echo $_SESSION['userName']; ?>"></p>
      <input type="hidden" name="userID" class="hidden" maxlength="255" size="50" value="<?php echo $_SESSION['userID']; ?>"></p>
      <!-- End of User ID Fields -->
      <p>Subject: <input type="text" name="subject" class="textField" id="subject" maxlength="255" size="50" required onchange="leftTrimSpecialCharacters('subject');"></p>
      <p><textarea name="content" class="largeTextArea" id="content" maxlength="5000" form="addNewPostForm" required onchange="leftTrimSpecialCharacters('content');"></textarea></p>

      <p><input type="submit" name="button" id="button" value="Add Post" class="blueButton"/>
      <input type="reset" name="button2" id="button2" value="Reset" class="blueButton"/></p>
</div>
</div>

    </form>

<?php include 'homeOwnersAssociationFooter.php'; ?>
