var numberOfAddedIngredients = 0;  //create one-way variable to add ingredients without overlap
var totalIngredients = 0;  //create variable to account for ingredients after removed ingredients
var removedIngredients = 0;
var newHeight = 300; //set div recipeBox initial height to 300
var addAnotherIngredient = 0;
var ingredient = "";
var anotherIngredientCounter = 0;
var totalAnotherIngredient = 0;
var numberOfAddedAnotherIngredient = 0;
var removedAnotherIngredients = 0;
var totalRecipes = 0;
var removedRecipes = 0;

function leftTrim(elementID){

    var position = 0;
    var leftTrimmedSubstring = "";
    var elementIDValue = document.getElementById(elementID).value;
    //while the character is a space up until the first character store 
    //that new position.  Use the new position to substring the value and
    //return it to the field.
    while (elementIDValue.charAt(position) == " "){
       
        position++;

    }

    leftTrimmedSubstring = elementIDValue.substring(position); 
    //alert(leftTrimmedSubstring);
    document.getElementById(elementID).value = leftTrimmedSubstring;
    
} 

function numericOnly(elementID){

    var position = 0;  
    var elementIDValue = document.getElementById(elementID).value;

    for (position = 0; position < elementIDValue.length; position++){
       //Check if the element is not a number at each position
        if(isNaN(elementIDValue.charAt(position))){
        //if it is not a number subtract one from the position and delete the new non-numeric number
        elementIDValue = elementIDValue.substring(0, (position - 1)) + elementIDValue.substring(0, (position - 1));
        //reset the field value
        document.getElementById(elementID).value = elementIDValue;
             
        }

        else{
        
        //do nothing
        document.getElementById(elementID).value = elementIDValue;
        
        }

}
}

function searchReplaceSpecialCharacter(elementID, symbol, HTMLEntity){
        
        //set elementIDValue
        var elementIDValue = document.getElementById(elementID).value;
        //Replace the symbol with the entity
        elementIDValue = elementIDValue.replace(symbol, HTMLEntity);
        //set the new field value
        document.getElementById(elementID).value = elementIDValue;

}

//create master function to call searchReplaceSpecialCharacter for each special character
function convertAllSpecialCharacters(elementID){

// Convert all the punctuation to their HTML Entity equivalent if it exists

         searchReplaceSpecialCharacter(elementID, "-", "&ndash;");
         searchReplaceSpecialCharacter(elementID, "—", "&mdash;");
         searchReplaceSpecialCharacter(elementID, "¡", "&iexcl;");
         searchReplaceSpecialCharacter(elementID, "¿", "&iquest;");
         searchReplaceSpecialCharacter(elementID, "\"", "&quot;");
         searchReplaceSpecialCharacter(elementID, "\“", "&ldquo;");
         searchReplaceSpecialCharacter(elementID, "\”", "&rdquo;");
         searchReplaceSpecialCharacter(elementID, "\'", "&#39;");
         searchReplaceSpecialCharacter(elementID, "‘", "&lsquo;");
         searchReplaceSpecialCharacter(elementID, "’", "&rsquo;");
         searchReplaceSpecialCharacter(elementID, "«", "&laquo;");
         searchReplaceSpecialCharacter(elementID, "»", "&raquo;");

// Convert all the symbols to their HTML Entity equivalent if it exists

         //searchReplaceSpecialCharacter(elementID, "&", "&amp;");
         searchReplaceSpecialCharacter(elementID, "¢", "&cent;");
         searchReplaceSpecialCharacter(elementID, "©", "&copy;");
         searchReplaceSpecialCharacter(elementID, "÷", "&divide;");
         searchReplaceSpecialCharacter(elementID, ">", "&gt;");
         searchReplaceSpecialCharacter(elementID, "<", "&lt;");
         searchReplaceSpecialCharacter(elementID, "µ", "&micro;");
         searchReplaceSpecialCharacter(elementID, "·", "&middot;");
         searchReplaceSpecialCharacter(elementID, "¶", "&para;");
         searchReplaceSpecialCharacter(elementID, "±", "&plusmn;");
         searchReplaceSpecialCharacter(elementID, "€", "&euro;");
         searchReplaceSpecialCharacter(elementID, "£", "&pound;");
         searchReplaceSpecialCharacter(elementID, "®", "&reg;");         
         searchReplaceSpecialCharacter(elementID, "§", "&sect;");
         searchReplaceSpecialCharacter(elementID, "™", "&trade;");
         searchReplaceSpecialCharacter(elementID, "¥", "&yen;");
         searchReplaceSpecialCharacter(elementID, "°", "&deg;");

}

function leftTrimNumeric(elementID){

    leftTrim(elementID);
    numericOnly(elementID);
    convertAllSpecialCharacters(elementID);

}

function leftTrimSpecialCharacters(elementID){

    leftTrim(elementID);

}


function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1);});
}


//end of validation functions

function addNewPost() {

    window.location.href = "homeOwnersAssociationAddPostForm.php";

}

function viewArticleAndByLawsDirect() {

    window.location.href = "articlesAndByLaws.php";

}

function viewBulletinBoardDirect() {

    window.location.href = "bulletinBoard.php";

}


/*function removePost(postNumber){
    
if ($("#addPostDiv")[0].innerHTML == ""){

    $("#addPostDiv").hide();

}

else { 

    $("#addPostDiv").show();

 }

    console.log("#postBox " + postNumber + " removed");
    $(("#postBox" + postNumber)).empty(); //clear html from ingredientbox
    $(("#postBox" + postNumber)).remove(); //clear html from ingredientbox
    totalPosts -= 1;
    removedPosts += 1;
    console.log("totalPosts = " + totalPosts);
    console.log("numberOfAddedPosts = " + numberOfAddedPosts);
    
}*/

function removeAnotherPost(postNumber){
    
    console.log("#anotherPostBox " + postNumber + " removed");
    $(("#anotherPostBox" + postNumber)).empty(); //clear html from ingredientbox
    $(("#anotherPostBox" + postNumber)).remove(); //clear html from ingredientbox
    totalAnotherPost -= 1;
    removedAnotherPosts += 1;
    numberOfAddedAnotherPost -= 1;
    console.log("totalAnotherPost = " + totalposts);
    console.log("numberOfAddedAnotherPosts = " + numberOfAddedAnotherPPostost);
    
}

function removePost(postNumber){
    
    console.log("#anotherPost " + postNumber + " removed");
    $(("#anotherPost" + postNumber)).empty(); //clear html from ingredientbox
    $(("#anotherPost" + postNumber)).remove(); //clear html from ingredientbox
    totalPosts -= 1;
    removedPosts += 1;
    console.log("totalPosts = " + totalPosts);
    
}


    function deleteUserDirect(recordId){

        window.location.replace("owners.php?recordId=" + recordId + "&userDeleteConfimationCheck=1");
        
    }

    function updateUserDirect(recordId){

        window.location.replace("updateUserForm.php?recordId=" + recordId + "&updateConfimationCheck=1");
        
    }

// User confirmation checks.

    function deleteUserYes(recordId){

        window.location.replace("owners.php?recordId=" + recordId + "&userDeleteConfimationCheck=2");
        
    }

    function deleteUserNo(recordId){

        window.location.replace("owners.php?recordId=" + recordId + "&userDeleteConfimationCheck=0");
        
    }

    function updateUserYes(recordId){

        window.location.replace("owners.php?recordId=" + recordId + "&userUpdateConfimationCheck=2");
        
    }

    function updateUserNo(recordId){

        window.location.replace("owners.php?recordId=" + recordId + "&userUpdateConfimationCheck=0");
        
    }

// Post Confirmation Checks


    function deletePostYes(recordId){

        window.location.replace("bulletinBoard.php?recordId=" + recordId + "&postDeleteConfimationCheck=2");
        
    }

    function deletePostNo(recordId){

        window.location.replace("bulletinBoard.php?recordId=" + recordId + "&postDeleteConfimationCheck=0");
        
    }

    function deletePostSearchYes(recordId){

        window.location.replace("homeOwnersAssociationSearch.php?recordId=" + recordId + "&postDeleteConfimationCheck=2");
        
    }

    function deletePostSearchNo(recordId){

        window.location.replace("homeOwnersAssociationSearch.php?recordId=" + recordId + "&postDeleteConfimationCheck=0");
        
    }

    function updatePostYes(recordId){

        window.location.replace("bulletinBoard.php?recordId=" + recordId + "&postUpdateConfimationCheck=2");
        
    }

    function updatePostNo(recordId){

        window.location.replace("bulletinBoard.php?recordId=" + recordId + "&postUpdateConfimationCheck=0");
        
    }

// Article Confirmation Checks

    function deleteArticleYes(recordId){

        window.location.replace("articlesAndByLaws.php?recordId=" + recordId + "&articleDeleteConfimationCheck=2");
        
    }

    function deleteArticleNo(recordId){

        window.location.replace("articlesAndByLaws.php?recordId=" + recordId + "&articleDeleteConfimationCheck=0");
        
    }

    function updateArticleYes(recordId){

        window.location.replace("articlesAndByLaws.php?recordId=" + recordId + "&articleUpdateConfimationCheck=2");
        
    }

    function updateArticleNo(recordId){

        window.location.replace("articlesAndByLaws.php?recordId=" + recordId + "&articleUpdateConfimationCheck=0");
        
    }

// By Law Confirmation Checks


    function deleteByLawYes(recordId){

        window.location.replace("articlesAndByLaws.php?recordId=" + recordId + "&byLawDeleteConfimationCheck=2");
        
    }

    function deleteByLawNo(recordId){

        window.location.replace("articlesAndByLaws.php?recordId=" + recordId + "&byLawDeleteConfimationCheck=0");
        
    }

    function updateByLawYes(recordId){

        window.location.replace("articlesAndByLaws.php?recordId=" + recordId + "&byLawUpdateConfimationCheck=2");
        
    }

    function updateByLawNo(recordId){

        window.location.replace("articlesAndByLaws.php?recordId=" + recordId + "&byLawUpdateConfimationCheck=0");
        
    }

// Update & Delete Posts

    // function deletePostDirect(recordId){

    //     window.location.replace("deletePosts.php?recordId=" + recordId + "&deleteConfimationCheck=1");
        
    // }

    function updatePostDirect(recordId){

        window.location.replace("updatePostForm.php?recordId=" + recordId + "&updateConfimationCheck=1");
        
    }

    // function deletePostYes(recordId){

    //     window.location.replace("deletePosts.php?recordId=" + recordId + "&deleteConfimationCheck=2");
        
    // }

    // function deletePostNo(recordId){

    //     window.location.replace("deletePosts.php?recordId=" + recordId + "&deleteConfimationCheck=0");
        
    // }

    // function updatePostYes(recordId){

    //     window.location.replace("deletePosts.php?recordId=" + recordId + "&updateConfimationCheck=2");
        
    // }

    // function updatePostNo(recordId){

    //     window.location.replace("deletePosts.php?recordId=" + recordId + "&updateConfimationCheck=0");
        
    // }


// Update & Delete Articles

    // function deleteArticleDirect(recordId){

    //     window.location.replace("deleteArticles.php?recordId=" + recordId + "&deleteConfimationCheck=1");
        
    // }

    function updateArticleDirect(recordId){

        window.location.replace("updateArticleForm.php?recordId=" + recordId + "&updateConfimationCheck=1");
        
    }

    // function deleteArticleYes(recordId){

    //     window.location.replace("deleteArticles.php?recordId=" + recordId + "&deleteConfimationCheck=2");
        
    // }

    // function deleteArticleNo(recordId){

    //     window.location.replace("deleteArticles.php?recordId=" + recordId + "&deleteConfimationCheck=0");
        
    // }

    // function updateArticleYes(recordId){

    //     window.location.replace("deleteArticles.php?recordId=" + recordId + "&updateConfimationCheck=2");
        
    // }

    // function updateArticleNo(recordId){

    //     window.location.replace("deleteArticles.php?recordId=" + recordId + "&updateConfimationCheck=0");
        
    // }

// Update & Delete By Laws

    // function deleteByLawDirect(recordId){

    //     window.location.replace("deleteByLaws.php?recordId=" + recordId + "&deleteConfimationCheck=1");
        
    // }

    function updateByLawDirect(recordId){

        window.location.replace("updateByLawForm.php?recordId=" + recordId + "&updateConfimationCheck=1");
        
    }

    // function deleteByLawYes(recordId){

    //     window.location.replace("deleteByLaws.php?recordId=" + recordId + "&deleteConfimationCheck=2");
        
    // }

    // function deleteByLawNo(recordId){

    //     window.location.replace("deleteByLaws.php?recordId=" + recordId + "&deleteConfimationCheck=0");
        
    // }

    // function updateByLawYes(recordId){

    //     window.location.replace("deleteByLaws.php?recordId=" + recordId + "&updateConfimationCheck=2");
        
    // }

    // function updateByLawNo(recordId){

    //     window.location.replace("deleteByLaws.php?recordId=" + recordId + "&updateConfimationCheck=0");
        
    // }

// Update & Delete Functions

    function deleteArticleDirect(recordId){

        window.location.replace("articlesAndByLaws.php?recordId=" + recordId + "&articleDeleteConfimationCheck=1");
        
    }

    function deleteByLawDirect(recordId){

        window.location.replace("articlesAndByLaws.php?recordId=" + recordId + "&byLawDeleteConfimationCheck=1");
        
    }

    function deletePostSearchDirect(recordId){

        window.location.replace("homeOwnersAssociationSearch.php?recordId=" + recordId + "&postDeleteConfimationCheck=1");
        
    }

    function deletePostDirect(recordId){

        window.location.replace("bulletinBoard.php?recordId=" + recordId + "&postDeleteConfimationCheck=1");
        
    }

    function updateDirect(recordId){

        window.location.replace("updateForm.php?recordId=" + recordId + "&updateConfimationCheck=1");
        
    }

    function deleteYes(recordId){

        window.location.replace("delete.php?recordId=" + recordId + "&deleteConfimationCheck=2");
        
    }

    function deleteNo(recordId){

        window.location.replace("delete.php?recordId=" + recordId + "&deleteConfimationCheck=0");
        
    }

    function updateYes(recordId){

        window.location.replace("updateForm.php?recordId=" + recordId + "&updateConfimationCheck=2");
        
    }

    function updateNo(recordId){

        window.location.replace("updateForm.php?recordId=" + recordId + "&updateConfimationCheck=0");
        
    }

function searchPosts(search){


}

function newPostDirect(){

        window.location.replace("homeOwnersAssociationAddPostForm.php");

}

function newArticleDirect(){

        window.location.replace("homeOwnersAssociationAddArticleForm.php");

}

function newByLawDirect(){

        window.location.replace("homeOwnersAssociationAddByLawsForm.php");

}