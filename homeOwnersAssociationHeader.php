<?php 

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

?>

<?

    global $titleValue; 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Renaissance Villas Home Owners Association
<?php 

if(isset($titleValue)){

    echo ": " . $titleValue;

    } 

else
    { 
        echo "!";
    } 
?> 
</title>
<?php include 'externalScripts.php'; ?>

<script src="homeOwnersAssociationFunctions.js"></script>

       <script type="text/javascript" charset="utf-8"></script>

<style>

.newRecipe p {

    text-align: center;

}

.nav > li > a{

    padding: 15px 8px;

}

.userLogIn {

    padding: 20px 8px;
    text-align: center;

}
</style>

</head>
<?php include 'homeOwnersAssociationNavigationMenu.php'; ?>
<script>

<?php 

if(isset($activePage)){

echo "$('#" . $activePage ."').toggleClass(\"active\");"; 

}
?>

</script>
<body>
<div class="bodyWrapper">
<?php include 'createHomeOwnersAssociationTables.php'; ?>
