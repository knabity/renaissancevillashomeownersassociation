<?php include 'loginRequired.php'; ?>
<?php 

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>
<?php  

if($_SESSION['validUser']=="yes")			//For a valid user that has set up a session
{
	$_SESSION['validUser'] = "";			//Set session variable to empty
	unset($_SESSION['validUser']);			//Unset the session variable
	session_destroy();						//destroy the session attached to this page
	header("Location:homeOwnersAssociationLogin.php");	//Redirect the user to the sign in page
	exit;									//If still here for some reason close the PHP session
}
else
{
	header("Location:homeOwnersAssociationLogin.php");	//Invalid user then send them to the login page
}

?>