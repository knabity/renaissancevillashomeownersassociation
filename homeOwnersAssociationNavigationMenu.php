<script src="SWFObject/swfobject.js"></script>
<link rel="icon" type="image/png" href="images/R.png">
<div class="navigationMenu">
<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="homeOwnersAssociationLogin.php">

      <img src="images/R.png" width="20px"></a>
  </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">

        <li class="inactive" id="homePage"><a href="index.php" >Home</a></li>
        <li class="inactive" id="bulletinBoard"><a href="bulletinBoard.php">Bulletin Board</a></li>
        <li class="inactive" id="articlesAndByLaws"><a href="articlesAndByLaws.php">Articles & Bylaws</a></li>
        <li class="inactive" id="calendar"><a href="calendar.php">Calendar</a></li>
        <li class="inactive" id="owners"><a href="owners.php">Owners</a></li>
        <li class="inactive" id="map"><a href="map.php">Map</a></li>
        <li class="inactive" id="contactUs"><a href="contactUs.php">Contact</a></li>

        <?php 
        
        /*        if(isset($_SESSION['validUser'])){

            if($_SESSION['validUser'] == "yes")
            
            {
            echo "<li class=\"inactive\" id=\"login\"><a href=\"homeOwnersAssociationLogout.php\">Logout</a></li>";
            }
            
            else{
            
            echo "<li class=\"inactive\" id=\"login\"><a href=\"homeOwnersAssociationLogin.php\">Login</a></li>";
            
            }

          }

        else {

            echo "<li class=\"inactive\" id=\"login\"><a href=\"homeOwnersAssociationLogin.php\">Login</a></li>";

        }*/

        ?>

<!--         
<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
  <ul class="dropdown-menu" role="menu">
    <li><a href="#">Action</a></li>
    <li><a href="#">Another action</a></li>
    <li><a href="#">Something else here</a></li>
    <li class="divider"></li>
    <li><a href="#">Separated link</a></li>
    <li class="divider"></li>
    <li><a href="#">One more separated link</a></li>
  </ul>
</li> -->

      </ul>
      <form class="navbar-form navbar-left" role="search" method="post" action="homeOwnersAssociationSearch.php" action="">
        <div class="form-group">
          <input type="text" class="form-control" name="searchhomeOwnersAssociation" id="searchhomeOwnersAssociation" placeholder="Search For Posts">
        </div>
        <button type="submit" class="btn btn-default"> <img src="Icons/searchIcon.png" height="20px"></button> 
            <script>
            function printPage() {
                window.print();
            }
            </script>

        <button type="button" class="btn btn-default"><img src="Icons/printerIcon.png" onclick="printPage();" height="20px"></button>

      </form>

      <ul class="nav navbar-nav navbar-right">

<?php 

      if(isset($_SESSION['validUser'])){

            if($_SESSION['validUser'] == "yes")
            
            {
                echo "<span class=\"userLogIn\">";
                echo "" . $_SESSION['userName'] . "<br />";
                echo "</span>";
                echo "<span class=\"userLogIn\">";
                echo "<a href=\"homeOwnersAssociationLogout.php\">Logout</a> <br />";
                echo "</span>";


            }
            
            else{

                echo "<span class=\"userLogIn\">";
                echo "<a href=\"homeOwnersAssociationLogin.php\">Login</a>";
                echo "</span>";

            }

      }
      
      else{

          echo "<span class=\"userLogIn\">";
          echo "<a href=\"homeOwnersAssociationLogin.php\">Login</a>";
          echo "</span>";

      }
          
?>

          </ul>
        </li>
      </ul>
        <br />
        <br />
        
        <!-- if this is the users first visit and their browser supports Flash output the Flash banner; otherwise, output the PNG -->
        <?php

        if(isset($_SESSION['firstVisit'])){
        
        ?>

            <script>

            document.write("<br /><br /><p><div class=\"titleTransparentWrapper\"><img src=\"images/RenaissanceVillasHomeOwnersAssociation.png\" width=\"90%\"></p></div></div></div>");

            </script>

        <?php

      }

      else {

      ?>

        <script>
        var flashValid = false;

        $('html').addClass(typeof swfobject !== 'undefined' && swfobject.getFlashPlayerVersion().major !== 0 ? 'flash' : 'no-flash');
        if( swfobject.hasFlashPlayerVersion("8.0") ) { 

            document.write("<div style=\"position:relative;\"><object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0\" width=\"100%\">");
            document.write("<param name=\"wmode\" value=\"transparent\">");
            document.write("<param name=\"movie\" value=\"images/RenaissanceVillasLogo.swf\" />");
            document.write("<embed src=\"images/RenaissanceVillasLogo.swf\" wmode=\"transparent\" quality=\"high\" type=\"application/x-shockwave-flash\" width=\"100%\" pluginspage=\"http://www.macromedia.com/go/getflashplayer\" />");
            document.write("<param name=\"quality\" value=\"high\" /></object>");
            document.write("</object>");
            document.write("<img src=\"images/FlashSpace.png\" width=\"90%\">");
            document.write("</div></div></div>");

            <?php $_SESSION['firstVisit'] = "firstVisit"; ?>

            flashValid = true;
        }

        else{

            document.write("<br /><br /><p><img src=\"images/RenaissanceVillasHomeOwnersAssociation.png\" width=\"90%\"></p></div></div>");

        }

        </script>

      <?php

        }

      ?>

</nav>
</div>
<script>
 $(window).scroll(function (e) {
        $menu = $('.navigationMenu');
        if ($(this).scrollTop() > 80 && $menu.css('position') != 'fixed') {
            $('.navigationMenu').css({ 'position': 'fixed', 'top': '-10px', 'left':'0px', 'right':'0'});
        }
        if ($(this).scrollTop() < 80 && $menu.css('position') == 'fixed') {
            $('.navigationMenu').css({ 'position': 'absolute', 'top': '0px', 'left':'', 'right':'' });
        }
    });

/*if(flashValid == true){

            document.write("<br /><br /><br /><br /><br />");

}*/
</script>

<br />
<br />
<br />
<br />
