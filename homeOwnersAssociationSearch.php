<?php include 'loginRequired.php'; ?>
<?php 
//if the keyword has been declared in the search bar, set it to variable $search
$search = "";

if(isset($_POST['searchhomeOwnersAssociation'])){

  global $search;
  $search = htmlspecialchars($_POST['searchhomeOwnersAssociation']); 

}

  ?>
<?php $titleValue = "Home Owners Association: Searching for " . $search; ?>
<?php include 'homeOwnersAssociationHeader.php'; ?>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />


<!-- Create script to delete a Post with a confirmation pop up. -->


<?php

if(isset($_GET['postDeleteConfimationCheck'])){
    
    if( $_GET['postDeleteConfimationCheck'] == 1 ){
            
            include 'dbConnect.php';
            $sql = "SELECT * FROM homeOwnersAssociationPostsTable WHERE homeOwnersAssociationPost_id = " . $_GET['recordId'];   //build the SQL query
                //Note the WHERE clause allows us to select ONLY the desired record
    
            //3.  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
          $result = mysqli_query($link,$sql);
            

          if(!$result )             //Make sure the Query ran correctly and created result
          {
            echo "<h1 style='color:red'>Houston, We have a problem!</h1>";  //Problems were encountered.
            echo mysqi_error($link);    //Display error message information
          }


            echo "<p align=\"center\"><div class=\"whiteWrapper\"><h1>Are you sure you want to delete the following record?</h1></p>"; 
            while($row = mysqli_fetch_array($result))   //Turn each row of the result into an associative array 
            {
                //Display the selected post.
                echo "<div id=\"deletingPost\" class=\"postWrapper\"><br />"; 
                echo "<div class=\"whiteWrapper\">";
                echo $row['content'];
                echo "</div>";     
                echo "Submitted by " . $row['userName'] . "<br />";
                echo "<br />Submission Date: " . $row['submission_date'];
                echo "<br />Submission Time: " . $row['submission_time'] . "<br />";  
                echo "<br /><br />";
                echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Yes\" onclick=\"deletePostSearchYes(" . $row['homeOwnersAssociationPost_id'] . ");\" />";
                echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"No\" onclick=\"deletePostSearchNo(" . $row['homeOwnersAssociationPost_id'] . ");\" />";
                echo "</div>";
                echo "</div>";
          }
            
                echo "<br /><br />";


        }
    elseif ($_GET['postDeleteConfimationCheck'] == 2 ){
    
        if(isset($_GET['recordId'])){
      
              $deleteRecId = $_GET['recordId'];   //Pull the event_id from the GET parameter
            
              include 'dbConnect.php';    //connects to the database
           
              $sqlSelect = "SELECT * FROM homeOwnersAssociationPostsTable";   //build the SQL query
                        //Note the WHERE clause allows us to select ONLY the desired record
            
                //  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
              $result = mysqli_query($link,$sqlSelect);   //run the Query and store the result in $result
            
              if(!$result )             //Make sure the Query ran correctly and created result
              {
                echo "<h1 style='color:red'>Something went wrong.</h1>";  //Problems were encountered.
                echo mysqi_error($link);    //Display error message information
              }
            
                include 'dbConnect.php';    //connects to the database
                echo "<div class=\"postWrapper\">";
                echo "<h2>Record number: " . $_GET['recordId'] . " has been deleted.</h2>"; //Display a message verifying the record to be deleted.  This could be turned into a second confirmation
                
                $sql = "DELETE FROM homeOwnersAssociationPostsTable WHERE homeOwnersAssociationPost_id = $deleteRecId";
                  //echo "<p>The SQL Command: $sql </p>";     //testing
              
                if (mysqli_query($link,$sql) )          //process the query
                {     

                echo "<h1>Your record has been successfully deleted.</h1>";
                echo "<p><a href='bulletinBoard.php'>Return to Bulletin Board</a></p>";  
                echo "</div>";
                echo "<br />";
                echo "<br />";
                
                }
                else
                {
                  echo "<h1>You have encountered a problem with your delete.</h1>";
                  echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
                }
            }
    
        }
    else {

        //append the table
        // include 'displayTable.php';
    
    }
}
else {

    //append the table
    // include 'displayTable.php';

}


?>


<?php


  include 'dbConnect.php';
  //2.  Create an SQL SELECT command that will pull all the posts from your posts table.
  $sql = "SELECT * FROM homeOwnersAssociationPostsTable WHERE content LIKE '%$search%' ORDER BY submission_date DESC, submission_time DESC";   //build the SQL query
            //Note the WHERE clause allows us to select ONLY the desired record

    //3.  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
  $result = mysqli_query($link,$sql);   //run the Query and store the result in $result

  if(!$result )             //Make sure the Query ran correctly and created result
  {

    echo "<h1 style='color:red'>There is a problem.</h1>";  //Problems were encountered.
    echo mysqi_error($link);    //Display error message information

  }

?>

<script>
$('#bulletinBoard').toggleClass("active");
</script>

<?php

?>

<div class="titleWrapper">

<input type="button" name="addNewPost" id="addNewPost" value="Add New Post" onclick="addNewPost();" />

</div>

<div class="bodyWrapper">

<?php

$postNumber = 0;


//4.  Use a PHP loop to process each row in the result.
  echo "<script>var postsPulledFromDatabase = " . mysqli_num_rows($result) . "; </script>";

  while($row = mysqli_fetch_array($result))   //Turn each row of the result into an associative array 
    {

      global $postNumber;

      echo "<div id=\"anotherPost" . $postNumber . "\" class=\"beigeBodyWrapper\"><br />"; 
      echo "<img src=\"Icons/redCancelCircle.svg\" title=\"Hide Post (Does Not Delete.  You Can Recover the Entry by Refreshing the Page)\" alt=\"Hide Post (Does Not Delete.  You Can Recover the Entry by Refreshing the Page)\" width=\"20px\" class = \"cancelButton\" onclick=\"removePost('" . $postNumber . "');\"><br />";
      $subjectValueRemoveSpaces = str_replace("-"," ",$row['subject']); //convert - to ""
      $contentValueRemoveSpaces = str_replace("-"," ",$row['content']); //convert - to ""
      //echo "<script>alert('" . $nameValueRemoveSpaces . "');</script>";
      echo "<div class=\"whiteWrapper\">";
      echo "<h1>" . $subjectValueRemoveSpaces . "</h1>";
      echo "</div>";
      echo "<div class=\"whiteWrapper\">";
      echo "<p>" . $contentValueRemoveSpaces . "</p>";
      echo "</div>";

      echo "<br />Submission Date: " . $row['submission_date'];
      echo "<br />Submission Time: " . $row['submission_time'] . "<br />";  
      echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Update\" onclick=\"updateDirect('" . $row['homeOwnersAssociationPost_id'] . "');\">";
      echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Delete\" onclick=\"deletePostSearchDirect('" . $row['homeOwnersAssociationPost_id'] . "');\">";
      echo "</div>";


              $postNumber += 1;


    }

?>
<script>


$(document).ready(function()
  {
  //$(".thumbnail").css("transform","scale(1,1) rotateZ(360deg) rotateX(360deg)");

$(".beigeWrapper").css({

  "-webkit-transform" : "rotateZ(360deg) rotateX(360deg) rotateY(360deg) scale(1,1)",
  "-moz-transform" : "rotateZ(360deg) rotateX(360deg) rotateY(360deg) scale(1,1)",
  "-ms-transform" : "rotateZ(360deg) rotateX(360deg) rotateY(360deg) scale(1,1)",
  "-o-transform" : "rotateZ(360deg) rotateX(360deg) rotateY(360deg) scale(1,1)",
  "transform" : "rotateZ(360deg) rotateX(360deg) rotateY(360deg) scale(1,1)",

});



});


$(document).ready(function()
  {
  //$(".thumbnail").css("transform","scale(1,1) rotateZ(360deg) rotateX(360deg)");



$(".thumbnail").css({

  "-webkit-transform" : "rotateZ(720deg) rotateX(720deg) rotateY(720deg) scale(1,1)",
  "-moz-transform" : "rotateZ(720deg) rotateX(720deg) rotateY(720deg) scale(1,1)",
  "-ms-transform" : "rotateZ(720deg) rotateX(720deg) rotateY(720deg) scale(1,1)", 
  "-o-transform" : "rotateZ(720deg) rotateX(720deg) rotateY(720deg) scale(1,1)",
  "transform" : "rotateZ(720deg) rotateX(720deg) rotateY(720deg) scale(1,1)",
  "width" : "100%"

});

});

</script>

<?php include 'homeOwnersAssociationFooter.php'; ?>
