<?php include 'loginRequired.php'; ?>
<?php $titleValue = "Users"; ?>
<?php include 'homeOwnersAssociationHeader.php'; ?>
<br />
<br />
<br />
<br />
<br />
<br />
<?php 

include 'dbConnect.php';

$test = "SHOW TABLES IN `wdv341` WHERE `Tables_in_tests` = 'homeOwnersAssociation'";

// perform the query and store the result
$result = $link->query($test);

if($result){

    echo "Table Exists";

    }

    else{


        $sql  = "CREATE TABLE event_user_seq";
        $sql  .= "(";
        $sql  .= "  user_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY";
        $sql  .= "  );";

        
        if ( mysqli_query($link,$sql) ){
                //create table wdv341_events
            $sql = "CREATE TABLE event_user";
            $sql .= "(";
            $sql .= "user_id VARCHAR(7) NOT NULL DEFAULT '0', name VARCHAR(30),";
            $sql .= "event_userName VARCHAR(255) NOT NULL,";
            $sql .= "event_password VARCHAR(255) NOT NULL,";
            $sql .= "submission_date DATE,";
            $sql .= "submission_time TIME,";
            $sql .= "PRIMARY KEY (user_id,submission_time)";
            $sql .= ");";

                if ( mysqli_query($link,$sql) ){
                    //it worked
    
                    $sql = "DELIMITER $$";
                    $sql .= "CREATE TRIGGER tg_event_user_insert";
                    $sql .= "BEFORE INSERT ON event_user";
                    $sql .= "FOR EACH ROW";
                    $sql .= "BEGIN";
                    $sql .= "INSERT INTO event_user_seq VALUES (NULL);";
                    $sql .= "SET NEW.id = CONCAT('Event :', LPAD(LAST_INSERT_ID(), 3, '0'));";
                    $sql .= "END$$";
                    $sql .= "DELIMITER ;";
    
                }
            
            else {

                //echo "delimiter didn't work.";

                  }
                }

        else

            {

                //do nothing
                //echo "<h1>You have encountered a problem.</h1>";
                //echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";

            }

}
//end of table creation

?>

<body>
<?php include 'recipeNavigationMenu.php'; ?>

<div class="whiteWrapper">
<h1>Add New User</h1>
</div>
<div class="whiteWrapper">
<h3>Input Form for Adding New Users</h3>

<form id="eventsForm" name="eventsForm" method="post" action="insertUser.php">
  <p>Add a new Presenter</p>

  <p>User Name:  
    <input type="text" name="event_userName" id="event_userName" />
  </p>
  <p>Password:  
    <input type="password" name="event_password" id="event_password" />
  </p>

  <p>
    <input type="submit" name="button" id="button" value="Add User" />
    <input type="reset" name="button2" id="button2" value="Clear Form" />
  </p>
</form>

<?php include 'recipeFooter.php'; ?>
