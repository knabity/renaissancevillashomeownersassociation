<?php $activePage = "homePage"; ?>
<?php include 'homeOwnersAssociationHeader.php'; ?>
<script type="text/javascript" src="jquery-slider-master/js/jssor.js"></script>
<script type="text/javascript" src="jquery-slider-master/js/jssor.slider.js"></script>
<script src="jquery-1.9.1.min.js"></script>                            
<script src="jssor.slider.mini.js"></script>
<script>
    jQuery(document).ready(function ($) {
        var options = {};                            
        var jssor_slider1 = new $JssorSlider$('slider1_container', options);

        //responsive code begin
        //you can remove responsive code if you don't want the slider scales
        //while window resizes
        function ScaleSlider() {
            var parentWidth = $('#slider1_container').parent().width();
            if (parentWidth) {
                jssor_slider1.$ScaleWidth(parentWidth);
            }
            else
                window.setTimeout(ScaleSlider, 30);
        }
        //Scale slider after document ready
        ScaleSlider();
                                        
        //Scale slider while window load/resize/orientationchange.
        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
        //responsive code end
    });
</script>

<?php

    if(isset($_SESSION['validUser'])){
        if($_SESSION['validUser'] == "yes")
        
            {

            //2.  Create an SQL SELECT command that will pull all the posts from posts table.
              $sql = "SELECT * FROM homeOwnersAssociationPostsTable ORDER BY submission_date DESC, submission_time DESC";   //build the SQL query
                        //Note the WHERE clause allows us to select ONLY the desired record
            
                //3.  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
              $result = mysqli_query($link,$sql);   //run the Query and store the result in $result
            
              if(!$result )             //Make sure the Query ran correctly and created result
              {
                echo "<h1 style='color:red'>There is a problem.</h1>";  //Problems were encountered.
                echo mysqi_error($link);    //Display error message information
              }
            ?>
            
            <div class="centerClass">
            
            <?php
            
            $postNumber = 0;
            
            
            //4.  Use a PHP loop to process each row in the result.
              echo "<script>var postsPulledFromDatabase = " . mysqli_num_rows($result) . "; </script>";
              echo "<br />";
              echo "<br />";
              echo "<br />";
              echo "<br />";
              echo "<br />";
              echo "<br />";
              echo "<br />";
              echo "<br />";
              echo "<br />";
              echo "<br />";
              echo "<br />";
              while($row = mysqli_fetch_array($result))   //Turn each row of the result into an associative array 
                {
            
                  global $postNumber;
            
                  echo "<div id=\"anotherPost" . $postNumber . "\" class=\"postWrapper\"><br />"; 
                  echo "<img src=\"Icons/redCancelCircle.svg\" width=\"20px\" class = \"cancelButton\" onclick=\"removePost('" . $postNumber . "');\"><br />";
                  $subjectValueRemoveSpaces = str_replace("-"," ",$row['subject']); //convert - to ""
                  //echo "<script>alert('" . $nameValueRemoveSpaces . "');</script>";
                  
                  echo "<div class=\"whiteWrapper\">";
                  echo "<h1>" . $subjectValueRemoveSpaces . "</h1>";
                  echo "</div>";
            
                  ?>
            
                  <?php
            
                          echo "<div class=\"whiteWrapper\">";
                          echo $row['content'];
                          echo "</div>";
                    
                          echo "Submitted by " . $row['userName'] . "<br />";
                          echo "<br />Submission Date: " . $row['submission_date'];
                          echo "<br />Submission Time: " . $row['submission_time'] . "<br />";  
                          if($_SESSION['userID'] != $row['userID']){
            
                            if($_SESSION['adminPrivileges'] != "1"){
                              //not valid user
                            }
            
                            else {
            
                                  echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Update\" onclick=\"updatePostDirect('" . $row['homeOwnersAssociationPost_id'] . "');\">";
                                  echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Delete\" onclick=\"deletePostDirect('" . $row['homeOwnersAssociationPost_id'] . "');\">";
            
                            }
                          }
                          else {
            
                                  echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Update\" onclick=\"updatePostDirect('" . $row['homeOwnersAssociationPost_id'] . "');\">";
                                  echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Delete\" onclick=\"deletePostDirect('" . $row['homeOwnersAssociationPost_id'] . "');\">";
            
            
                          }
            
                          echo "</div>";
            
            
                          $postNumber += 1;
            
                }
                          
        
        }
        
        else{
        
?>
                    <br />
                    <br />
                    <br />
                    <br />

                    <div id="slider1_container" style="position: relative; width: 100%;">
                        <!-- Slides Container -->
                        <div u="slides" style="cursor: move; width: 100%;
                            overflow: hidden;">
                            <div><img u="image" src="images/RenaissanceVillasSign.jpg" width="100%"/></div>
                            <div><img u="image" src="images/RenaissanceVillasHouses1.jpg" width="100%"/></div>
                            <div><img u="image" src="images/RenaissanceVillasHouses2.jpg" width="100%"/></div>
                            <div><img u="image" src="images/RenaissanceVillasHouses7.jpg" width="100%"/></div>
                        </div>
                    </div>

<?php
        }
}
    else{


?>
<br />
<br />
<br />
<br />
<div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 600px; height: 300px; overflow: hidden;">
    <!-- Slides Container -->
    <div u="slides" style="cursor: move; position: absolute; overflow: hidden; left: 0px; top: 0px; width: 600px; height: 300px;">
        <div><img u="image" src="images/RenaissanceVillasSign.jpg" /></div>
        <div><img u="image" src="images/RenaissanceVillasHouses1.jpg" /></div>
        <div><img u="image" src="images/RenaissanceVillasHouses2.jpg" /></div>
        <div><img u="image" src="images/RenaissanceVillasHouses7.jpg" /></div>

    </div>
    <!-- Trigger -->
    <script>jssor_slider1_starter('slider1_container');</script>

<?php

    }
        ?>
                      
<?php include 'homeOwnersAssociationFooter.php'; ?>
