<?php include 'loginRequired.php'; ?>
<?php $titleValue = "Add New User"; ?>
<?php include 'homeOwnersAssociationHeader.php'; ?>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<?php

//Get the name value pairs from the $_POST variable into PHP variables
//For this example I am using PHP variables with the same name as the name atribute from the HTML form
$firstName = htmlspecialchars($_POST['firstName']);
$lastName = htmlspecialchars($_POST['lastName']);
$userName = htmlspecialchars($_POST['userName']);
$emailAddress = htmlspecialchars($_POST['emailAddress']);
$streetAddress = htmlspecialchars($_POST['streetAddress']);
$cityStateZip = htmlspecialchars($_POST['cityStateZip']);
$phoneNumber = htmlspecialchars($_POST['phoneNumber']);
$listPhone = htmlspecialchars($_POST['listPhone']);

if(isset($_POST['adminPrivileges'])){

    $adminPrivileges = htmlspecialchars($_POST['adminPrivileges']);

}

else{

    $adminPrivileges = 0;

}

$password = htmlspecialchars($_POST['password']);
$submission_date = date('Y-m-d');
$submission_time = date("H:i:s");

	$sql = "INSERT INTO  homeOwnersAssociationOwnersTable (";
	$sql .= "firstName, ";
	$sql .= "lastName, ";
	$sql .= "userName, ";
	$sql .= "emailAddress, ";
	$sql .= "streetAddress, ";
	$sql .= "cityStateZip, ";
	$sql .= "phoneNumber, ";
	$sql .= "listPhone, ";
	$sql .= "adminPrivileges, ";
	$sql .= "password, ";
	$sql .= "submission_date, ";
	$sql .= "submission_time ";		
	//Last column in the list does NOT have a comma after it.

	$sql .= ") VALUES (";
	$sql .= "'$firstName', ";
	$sql .= "'$lastName', ";
	$sql .= "'$userName', ";
	$sql .= "'$emailAddress', ";
	$sql .= "'$streetAddress', ";
	$sql .= "'$cityStateZip', ";
	$sql .= "'$phoneNumber', ";
	$sql .= "'$listPhone', ";
	$sql .= "'$adminPrivileges', ";
	$sql .= "'$password', ";
	$sql .= "'$submission_date', ";
	$sql .= "'$submission_time' ";
	//Last column in the list does NOT have a comma after it.

	$sql .= ");";
	//Test the SQL command to see if it correctly formatted.

?>

<div class="beigeBodyWrapper80">
<h1>User Registration</h1>
</div>
<div class="beigeBodyWrapper80">
<?php
//Run the SQL command using the database you connected with
if (mysqli_query($link,$sql) )
{
	echo "<h1>Your record has been successfully added to the database.</h1>";
	echo "<p><a href='owners.php'>View Owners</a></p>";
}
else
{
	echo "<h1>You have encountered a problem.</h1>";
	echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
}

//Create confirmation Email


$body = "Thank You" . htmlspecialchars($_POST["firstName"]) . " " . htmlspecialchars($_POST["lastName"]) . ".  You are now registered with the Renaissance Villas Homeowner's Association";

/*

$body = "<!DOCTYPE html>";
$body .= "<head>";
$body .= "<title>Renaissance Villas Home Owners Assocation: Thank You for your registration.</title>";
$body .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://www.thenabsta.com/DMACC/PHP/HomeOwnersAssociation/homeOwnersAssociationStyleSheet.css\">";
$body .= "</head>";
$body .= "<html>";
$body .= "<body>";
$body .= "<div id=\"titleBackground\"><img src=\"http://www.thenabsta.com/DMACC/PHP/HomeOwnersAssociation/HomeOwnersAssociation.png\" width=\"100%\"></div>";
$body .= "<div id=\"whiteBackground\">";
$body .= "<h2>Thank you for your submission " .  htmlspecialchars($_POST["firstName"]) . " " . htmlspecialchars($_POST["lastName"]) . ".</h2>"; 


//check subscription checkboxes and out put a sentence saying whether or not the user has subscribed

$body.= "</div>";
$body.= "<div id=\"whiteBackground\"><h2>The time of submission is:</h2>" . date('l jS, \of F Y h:i:s A') . "</div>";
$body .= "</html>";
*/    //set up email address variable $to
	$to = htmlspecialchars($_POST["emailAddress"]);
	//set up $subject variable		
 	$subject = "Your Subscription to The Renaissance Villas Home Owners Association";
    $headers = 'From: noreply@RenaissanceVillasHomeOwnersAssociation.com' . "\r\n";
    $headers .= 'Reply-To: admin@RenaissanceVillasHomeOwnersAssociation.com' . "\r\n";

 	if (mail($to, $subject, $body, $headers)) 	//puts pieces together and emails
	{
   		echo("<div id=\"whiteBackground\"><h2><p>A message has been sent to your inbox!</p></h2></div>");
  	} 
	else 
	{
   		echo("<div id=\"whiteBackground\"><h2><p>Message delivery failed...</p></h2></div>");
  	}

$toAdmin = "thenabsta@gmail.com";

    mail($toAdmin, $subject, $body, $headers);  //puts pieces together and emails the admin

mysqli_close($link);	//closes the connection to the database once this page is complete.

?>




</div>
<?php include 'homeOwnersAssociationFooter.php'; ?>
