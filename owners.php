<?php include 'loginRequired.php'; ?>
<?php $titleValue = "Home Owners Association: Owners"; ?>
<?php $activePage = "owners"; ?>
<?php //include 'loginRequired.php'; ?>
<?php include 'homeOwnersAssociationHeader.php'; ?>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />

<!-- Create script to delete a user with a confirmation pop up. -->

<?php

if(isset($_GET['userDeleteConfimationCheck'])){
    
    if( $_GET['userDeleteConfimationCheck'] == 1 ){
            
            include 'dbConnect.php';
            $sql = "SELECT * FROM homeOwnersAssociationOwnersTable WHERE homeOwnersAssociationOwner_id = " . $_GET['recordId'];   //build the SQL query
                //Note the WHERE clause allows us to select ONLY the desired record
    
            //3.  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
          $result = mysqli_query($link,$sql);
            

          if(!$result )             //Make sure the Query ran correctly and created result
          {
            echo "<h1 style='color:red'>Houston, We have a problem!</h1>";  //Problems were encountered.
            echo mysqi_error($link);    //Display error message information
          }


            echo "<p align=\"center\"><div><h1>Are you sure you want to delete the following record?</h1></p>"; 
            while($row = mysqli_fetch_array($result))   //Turn each row of the result into an associative array 
            {
                //Display the selected user.
                echo "<div id=\"deletingUser\" class=\"postWrapper\"><br />"; 
                echo "<div class=\"whiteWrapper\">";
                echo "<h1>" . $row['userName'] . "</h1><br />";
                echo "<strong>First Name: </strong>" . $row['firstName'] . "<br />";
                echo "<strong>Last Name: </strong>" . $row['lastName'] . "<br />";
                echo "<strong>Email Address: </strong>" . $row['emailAddress'] . "<br />";
                echo "<br /><br />";
                echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Yes\" onclick=\"deleteUserYes(" . $row['homeOwnersAssociationOwner_id'] . ");\" />";
                echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"No\" onclick=\"deleteUserNo(" . $row['homeOwnersAssociationOwner_id'] . ");\" />";
                echo "</div>";
                echo "</div>";
                echo "</div>";
                
                include 'homeOwnersAssociationFooter.php';

          }
            
                echo "<br /><br />";


        }

    //This executes if the deletion has been confirmed
    elseif ($_GET['userDeleteConfimationCheck'] == 2 ){
    
        if(isset($_GET['recordId'])){
      
              $deleteRecId = $_GET['recordId'];   //Pull the event_id from the GET parameter
            
              include 'dbConnect.php';    //connects to the database
           
              $sqlSelect = "SELECT * FROM homeOwnersAssociationOwnersTable";   //build the SQL query
                        //Note the WHERE clause allows us to select ONLY the desired record
            
                //  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
              $result = mysqli_query($link,$sqlSelect);   //run the Query and store the result in $result
            
              if(!$result )             //Make sure the Query ran correctly and created result
              {
                echo "<h1 style='color:red'>Something went wrong.</h1>";  //Problems were encountered.
                echo mysqi_error($link);    //Display error message information
              }
            
                include 'dbConnect.php';    //connects to the database
                echo "<div class=\"postWrapper\">";
                echo "<h2>Record number: " . $_GET['recordId'] . " has been deleted.</h2>"; //Display a message verifying the record to be deleted.  This could be turned into a second confirmation
                
                $sql = "DELETE FROM homeOwnersAssociationOwnersTable WHERE homeOwnersAssociationOwner_id = $deleteRecId";
                  //echo "<p>The SQL Command: $sql </p>";     //testing
              
                if (mysqli_query($link,$sql) )          //process the query
                {     

                echo "<h1>Your record has been successfully deleted.</h1>";
                echo "<p><a href='owners.php'>Return to Owners</a></p>";  
                echo "</div>";
                include 'homeOwnersAssociationFooter.php';
                
                }
                else
                {
                  echo "<h1>You have encountered a problem with your delete.</h1>";
                  echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
                }
            }
    
        }
    else {

        //append the table
        include 'displayUserTable.php';
    
    }
}
else {

    //append the table
    include 'displayUserTable.php';

}


?>

