<?php include 'loginRequired.php'; ?>
<?php $activePage = "addNewPost"; ?>
<?php $titleValue = "Your Post Has Been Added"; ?>
<?php include 'homeOwnersAssociationHeader.php'; ?>
<?php include 'createHomeOwnersAssociationTables.php'; ?>
<body>
<br />
<br />
<br />
<br />
<br />
<div class="buttonWrapper">
<h1>
<input type="button" class = "blueButton" name="addNewPost" id="addNewPost" value="Add New Post" onclick="newPostDirect();">
<input type="button" class = "blueButton" name="viewBulletinBoard" id="viewBulletinBoard" value="View Posts" onclick="viewBulletinBoardDirect();"></h1>
</div>
<?php

include 'dbConnect.php';    //connects to the database

        $userName = htmlspecialchars($_POST['userName']);
        $userID = htmlspecialchars($_POST['userID']);
        $subject = htmlspecialchars($_POST['subject']);
        $content = htmlspecialchars($_POST['content']);
        $expiration_date = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d') + 60, date('Y')));
        $submission_date = date('Y-m-d');
        $submission_time = date("H:i:s");

        $sql = "INSERT INTO homeOwnersAssociationPostsTable (";
        $sql .= "userName, ";
        $sql .= "userID, ";
        $sql .= "subject, ";
        $sql .= "content, ";
        $sql .= "expiration_date, ";
        $sql .= "submission_date, ";
        $sql .= "submission_time ";     
        //Last column in the list does NOT have a comma after it.
    
        $sql .= ") VALUES (";
        $sql .= "'$userName', ";
        $sql .= "'$userID', ";
        $sql .= "'$subject', ";
        $sql .= "'$content', ";
        $sql .= "'$expiration_date', ";
        $sql .= "'$submission_date', ";
        $sql .= "'$submission_time' ";
        //Last column in the list does NOT have a comma after it.
    
        $sql .= ");";

        if (mysqli_query($link,$sql) )
        {
        echo "<div class=\"beigeBodyWrapper80\">";
        echo "<h2>Your Post in regards to " . $subject . " has been successfully added.</h2>";
        echo "</div>";
        }
        else
        {
            /*echo "<h1>You have encountered a problem.</h1>";
            echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";*/
        }
    
?>

<?php

mysqli_close($link);    //closes the connection to the database once this page is complete.

?>
<?php include 'homeOwnersAssociationFooter.php'; ?>
</body>
</html>