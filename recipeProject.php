<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Recipes Galore!</title>
<?php include 'externalScripts.php'; ?>
<?php include 'dbConnect.php'; ?>   <!-- Connect to Database -->
<?php include 'createRecipeTables.php'; ?>
<link rel="stylesheet" type="text/css" href="recipeProjectStyleSheet.css">
<script src="recipeProjectFunctions.js"></script>



       <script type="text/javascript" charset="utf-8">
    //   $(function(){
        
    //     window.f = new flux.slider('#slider', {
    //       autoplay: false,
    //       pagination: false
    //     });
        
    //     // Setup a listener for user requested transitions
    //     $('div.transitions').bind('click', function(event){
    //       event.preventDefault();

    //       // If this is a 3D transform and the browser doesn't support 3D then inform the user

          
    //       window.f.next(event.target.href.split('#')[1]);
    //     });
    //   });
     </script>

<!--       echo "$(.thumbnail).animate({";
echo "transform: 'translateX(50px)'";
echo "});";
//echo "$(\".thumbnail\").css(\"-webkit-transform\",\"scale(1,1) rotateZ(360deg) rotateX(360deg)\");";
     -->
</head>

<?php

  include 'dbConnect.php';
  //2.  Create an SQL SELECT command that will pull all the recipes from your recipes table.
  $sql = "SELECT * FROM recipeTable";   //build the SQL query
            //Note the WHERE clause allows us to select ONLY the desired record

    //3.  Process the SQL command and create a result.  It will include error handling in case your SELECT fails to run properly or the table is empty.
  $result = mysqli_query($link,$sql);   //run the Query and store the result in $result

  if(!$result )             //Make sure the Query ran correctly and created result
  {
    echo "<h1 style='color:red'>There is a problem.</h1>";  //Problems were encountered.
    echo mysqi_error($link);    //Display error message information
  }
?>

<?php include 'recipeNavigationMenu.php'; ?>

<script>
$('#homePage').toggleClass("active");
</script>

<?php

?>

<div class="whiteWrapper">

<img src="recipesGalore.png" width="100%">

</div>

<div class="whiteWrapper">

<input type="button" name="addNewRecipe" id="addNewRecipe" value="Add New Recipe" onclick="addNewRecipe();" />

</div>

<div class="whiteWrapper">

<?php

$recipeNumber = 0;


//4.  Use a PHP loop to process each row in the result.
  echo "<script>var recipesPulledFromDatabase = " . mysqli_num_rows($result) . "; </script>";

  while($row = mysqli_fetch_array($result))   //Turn each row of the result into an associative array 
    {

      global $recipeNumber;

      echo "<div id=\"anotherRecipe" . $recipeNumber . "\" class=\"beigeWrapper\"><br />"; 
      echo "<img src=\"Icons/redCancelCircle.svg\" width=\"20px\" class = \"cancelButton\" onclick=\"removeRecipe('" . $recipeNumber . "');\"><br />";
      $nameValueRemoveSpaces = str_replace("-"," ",$row['recipeName']); //convert - to ""
      //echo "<script>alert('" . $nameValueRemoveSpaces . "');</script>";
      echo "<h1>" . $nameValueRemoveSpaces . "</h1>";
      echo "<div class=\"whiteWrapper\">";
      // echo "<div id=\"slider\">";
      echo "<img src=\"images/" . $row['productImage'] . "\" class=\"thumbnail\" \" width=\"100%\" />";
      // echo "</div>";

      ?><!-- 
        <div class="transitions">
          <h2>2D Transitions</h2>
          <ul id="trans2D">
            <li><a href="#bars">Bars</a></li>
            <li><a href="#zip">Zip</a></li>
            <li><a href="#blinds">Blinds</a></li>
            <li><a href="#blocks">Blocks</a></li>
            <li><a href="#concentric">Concentric</a></li>
            <li><a href="#warp">Warp</a></li>
            <li><a href="#slide">Slide</a></li>
          </ul>
          <h2>3D Transitions</h2>
          <ul id="trans3d">
            <li><a href="#bars3d">Bars3D</a></li>
            <li><a href="#cube">Cube</a></li>
            <li><a href="#tiles3d" class="new">Tiles3D</a></li>
            <li><a href="#blinds3d" class="new">Blinds3D</a></li>
            <li><a href="#turn" class="new">Turn</a></li>
          </ul>
      </div> -->

      <?php

/*      echo "$(\".beigeWrapper\").css({";
      echo     "\"-webkit-transform\" : \"scale(1,1) rotateZ(360deg) rotateX(360deg)\",";
      echo     "\"-moz-transform\" : \"scale(1,1) rotateZ(360deg) rotateX(360deg)\",";
      echo     "\"-ms-transform\" : \"scale(1,1) rotateZ(360deg) rotateX(360deg)\",";
      echo     "\"-o-transform\" : \"scale(1,1) rotateZ(360deg) rotateX(360deg)\",";
      echo     "\"transform\" : \"scale(1,1) rotateZ(360deg) rotateX(360deg)\"";
      echo "});";*/


        $ingredientNameArray = explode(",", $row['ingredientNameArray']);
   
          echo "<ul class=\"list-group\">";

              echo "<h2>Ingredients</h2>";
          
          $instance = 0;
          $valueRemoveSpaces = "";
          foreach ($ingredientNameArray as &$value) {

              global $instance;
              global $valueRemoveSpaces;
              $valueRemoveSpaces = str_replace("-","",$value); //convert - to ""
              $valueRemoveHyphens = str_replace("-"," ",$value); //convert - to ""
              echo "<div class=\"panel panel-default\">";         
              echo "<li class=\"list-group-item\">" . $valueRemoveHyphens;
              $measurementArray = explode(",", $row['measurementArray']);
              echo "<script>";
              echo "var measurementArray" . $recipeNumber . " = [" . implode(", ", $measurementArray) . "];";  //echo measurement Array to Javascript
              //echo "var measurementUnitsArray = [" . implode(", ", $measurementUnitsArray) . "];";  //echo measurementUnitsArray to Javascript
              echo "</script>";
              $measurementUnitsArray = explode(",", $row['measurementUnitsArray']);
              echo "<span class=\"badge\">";
              echo "<span id=\"ingredientMeasurement" . $recipeNumber . $valueRemoveSpaces . "\">" . $measurementArray[$instance] . "</span> ";
              echo $measurementUnitsArray[$instance];
              echo "</span>";
              echo "</li>";
              $instance +=1;
              echo "</div>";
              
              }


// amount requested/new amount requested * quantity of ingredient
              echo "</div>";
              echo "<div class=\"whiteWrapper\">";
              echo $row['recipeInstructions'];
              echo "</div>";
              $instanceNew = $instance - 1;  //find length of measurement array
              $measurementArrayInstance = $measurementArray[$instanceNew];
              $currentServings = $row['numberOfServings'];
              echo "<script>";

              echo  "function calculateServings" . $recipeNumber . "(recipeNumber, instance){";
              echo          "var recipeNumber = parseInt(recipeNumber);";
              echo          "var instance = parseInt(instance) + 1;";
              echo          "var currentRecipeMeasurement;";
              echo          "var servingFactor;";
              echo          "var calculatedValue;";
              echo          "var numberOfServing=" . $currentServings . ";";
              // echo          "alert(numberOfServing);";
              echo          "var newNumberOfServings = $('#numberOfServings" . $recipeNumber . "').val();";
              // echo          "alert(newNumberOfServings);";
              echo          "var servingFactor = newNumberOfServings/numberOfServing;";
              // echo          "alert(servingFactor);";

        /*                    if($instanceNew == 0 ){
                                        foreach ($ingredientNameArray as &$value) {
                                        $valueRemoveSpaces = str_replace("-","",$value); //convert - to ""
              echo                      "currentRecipeMeasurement = $('ingredientMeasurement" . $recipeNumber . $valueRemoveSpaces . "').val();";
              echo                      "calculatedValue = currentRecipeMeasurement * servingFactor;";
              echo                      "$('#ingredientMeasurement" . $recipeNumber . $valueRemoveSpaces . "').html(calculatedValue);";
                                                }
                                        } 
                            else{*/
                                        foreach ($ingredientNameArray as &$value) {
                                        global $recipeNumber;
                                        $valueRemoveSpaces = str_replace("-","",$value); //convert - to ""
              echo                      "currentRecipeMeasurement = $('#ingredientMeasurement" . $recipeNumber . $valueRemoveSpaces . "').html();";
              echo                      "currentRecipeMeasurement = parseFloat(currentRecipeMeasurement);";
              echo                      "calculatedValue = currentRecipeMeasurement * servingFactor;";
              echo                      "$('#ingredientMeasurement" . $recipeNumber . $valueRemoveSpaces . "').html(calculatedValue);";
                                        }
                                 
              echo   "}"; //End of function

              echo "</script>";


        
              echo "Number of Servings: <select name=\"numberOfServings" . $recipeNumber . "\" id=\"numberOfServings" . $recipeNumber . "\" onChange=\"calculateServings" . $recipeNumber . "(" .  $recipeNumber . ", " . $instance . ");\">";
              echo "<option selected=\"selected\">" . $row['numberOfServings'] . "</option>";
        
              //create for loop to output numbers
              for ($i = 1; $i <= 100; $i++) {
                  echo "<option value=\"" . $i . "\">" . $i . "</option>";
              }
        
              echo "</select><br />";
        
              echo "Cooking Time: " . $row['cookingTime'] . "<br />";
              echo "Submitted by " . $row['author'] . "<br />";
              echo "<br />Submission Date: " . $row['submission_date'];
              echo "<br />Submission Time: " . $row['submission_time'] . "<br />";  
              echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Update\" onclick=\"updateDirect('" . $row['recipe_id'] . "');\">";
              echo "<input type=\"button\" name=\"button\" id=\"button\" value=\"Delete\" onclick=\"deleteDirect('" . $row['recipe_id'] . "');\">";
              echo "</div>";


              $recipeNumber += 1;
              $instance++;



    }
              
/*              echo "<script>";
              echo "var numberOfServings " . $recipeNumber . " = " . $row['numberOfServings'] . ";";
              echo "var newNumberOfServings " . $recipeNumber . " = numberOfServings;";
              echo "</script>";      */        


?>
<script>
/*      $('.thumbnail').animate({
      transform: 'rotateZ(234deg)'
      });*/
/*$( document ).ready(
  $('.thumbnail').css("-webkit-transform","scale(1,1) rotateZ(360deg) rotateX(360deg)");
  );*/

/*$( document ).ready(
  );
*/

$(document).ready(function()
  {
  //$(".thumbnail").css("transform","scale(1,1) rotateZ(360deg) rotateX(360deg)");

$(".beigeWrapper").css({

  "-webkit-transform" : "rotateZ(360deg) rotateX(360deg) rotateY(360deg) scale(1,1)",
  "-moz-transform" : "rotateZ(360deg) rotateX(360deg) rotateY(360deg) scale(1,1)",
  "-ms-transform" : "rotateZ(360deg) rotateX(360deg) rotateY(360deg) scale(1,1)",
  "-o-transform" : "rotateZ(360deg) rotateX(360deg) rotateY(360deg) scale(1,1)",
  "transform" : "rotateZ(360deg) rotateX(360deg) rotateY(360deg) scale(1,1)",
  "width" : "100%"

});



});


$(document).ready(function()
  {
  //$(".thumbnail").css("transform","scale(1,1) rotateZ(360deg) rotateX(360deg)");



$(".thumbnail").css({

  "-webkit-transform" : "rotateZ(720deg) rotateX(720deg) rotateY(720deg) scale(1,1)",
  "-moz-transform" : "rotateZ(720deg) rotateX(720deg) rotateY(720deg) scale(1,1)",
  "-ms-transform" : "rotateZ(720deg) rotateX(720deg) rotateY(720deg) scale(1,1)", 
  "-o-transform" : "rotateZ(720deg) rotateX(720deg) rotateY(720deg) scale(1,1)",
  "transform" : "rotateZ(720deg) rotateX(720deg) rotateY(720deg) scale(1,1)",
  "width" : "100%"

});

});
//$(".beigeWrapper").animate({width:"100%"},400);

/*
$(document).ready(function()
  {
    $(".thumbnail").animate({width:"100%"});
});
*/

</script>

</div>
 <div class="whiteWrapper">
    <a href="../../index.html">Back</a>
</div>
</div>  






<!-- 




<body>
<h1>WDV321 Advanced Javascript</h1>

</ul>
<p>The preparation instructions should:</p>
<ul>
  <li>Show when the customer clicks on something to show the instructions.</li>
  <li>Make sure any quantity changes are reflected in this area as well.</li>
  <li>Be able to close the instructions.</li>
</ul>
<p>A sample recipe has been provided. You are welcome to provide your own:</p>
<p>Crockpot Chili</p>
<p>Image: Not provided</p>
<p>Serves: 6</p>
<p>Preparation: 25 minutes</p>
<p>Cooking: 6 hours</p>
<p>Ingredients:</p>
<p>2 tbsp. cooking oil</p>
<p>1 cup onion</p>
<p>1 cup chopped peppers</p>
<p>4 tbsp. Chili powder</p>
<p>1 tsp. Hot chili powder (optional)</p>
<p>1 lb ground beef or chicken</p>
<p>2 cans Red Beans</p>
<p>2 cans Kidney Beans</p>
<p>2 cans Tomato Puree</p>
<p>2 cans Tomato Sauce</p>
<p>1 cup shredded cheese (optional)</p>
<p>1/2 cup sour cream (optional)</p>
<p>Insructions:</p>
<p>Heat cooking oil in 2 quart skillet.</p>
<p>Saute onions and peppers for 5 minutes.</p>
<p>Add spices and stir for 30 seconds.</p>
<p>Add meat and cook until browned. Approximately 15 minutes.</p>
<p>Pour contents of skillet into 3 quart crock pot.</p>
<p>Rinse beans and place in crockpot.</p>
<p>Open and pour Tomato puree and sauce into crock pot.</p>
<p>Cover crockpot and cook on low for 6 hours. </p>
<p>Serve into individual bowls and top with sour cream and cheese.</p>
</body>
</html>
 -->