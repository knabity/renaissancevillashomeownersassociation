<?php include 'loginRequired.php'; ?>
<?php $activePage = "addNewRecipes"; ?>
<?php $titleValue = "Your Recipe Has Been Added"; ?>
<?php include 'recipesGaloreHeader.php'; ?>
<?php include 'createRecipeTables.php'; ?>

<?php

include 'dbConnect.php';    //connects to the database

//Get the name value pairs from the $_POST variable into PHP variables
//For this example I am using PHP variables with the same name as the name atribute from the HTML form

//post the amount of added ingredients and the amount of new ingredients to global variables
$newIngredients = htmlspecialchars($_POST['newIngredients']);
$addAnotherIngredientForSubmit = htmlspecialchars($_POST['addAnotherIngredientForSubmit']);
//initialize global arrays to store ingredients along with their measurements
$ingredientNameArray = array();
$measurementUnitsArray = array();
$measurementArray = array();

/* example 1 */

    /*${"$measurementUnits" . $i} = htmlspecialchars($_POST['{measurementUnits . $i}']);
    ${"$measurement" . $i} = htmlspecialchars($_POST['{measurement . $i}']);
*/


    //find if there were any new ingredients added.  If so, add them to the ingredients table.  Also push the ingredients along with their measurement units and measurements into arrays to be stored in the Recipe Table.
    if ($newIngredients > 0) {
    
        global $newIngredients;
        global $addAnotherIngredientForSubmit;
        global $ingredientNameArray;
        global $measurementUnitsArray;
        global $measurementArray;

        for ($i = 0; $i < $newIngredients; $i++) {
        $ingredientNumber = "ingredient" . $i;
        $measurementUnitsNumber = "measurementUnitsNewIngredient" . $i;
        $measurementNumber = "measurementNewIngredient" . $i;


        $ingredientName = htmlspecialchars($_POST[$ingredientNumber]);
        $ingredientName = str_replace(" ","-",$ingredientName);
        $measurementUnits = htmlspecialchars($_POST[$measurementUnitsNumber]);
        $measurement = htmlspecialchars($_POST[$measurementNumber]);
        $submission_date = date('Y-m-d');
        $submission_time = date("H:i:s");
    
        $sql = "INSERT INTO ingredientTable (";
        $sql .= "ingredientName, ";
        $sql .= "submission_date, ";
        $sql .= "submission_time ";     
        //Last column in the list does NOT have a comma after it.
    
        $sql .= ") VALUES (";
        $sql .= "'$ingredientName', ";
        $sql .= "'$submission_date', ";
        $sql .= "'$submission_time' ";
        //Last column in the list does NOT have a comma after it.
    
        $sql .= ");";
        //Test the SQL command to see if it correctly formatted.
        //echo "<p>$sql</p>";
    
        //Push the ingredients along with their measurements into arrays 
        array_push($ingredientNameArray,$ingredientName);
        array_push($measurementUnitsArray,$measurementUnits);
        array_push($measurementArray,$measurement);

        if (mysqli_query($link,$sql) )
        {
        echo "<h1>Your Ingredient " . $ingredientName . " has been successfully added to the database.</    h1>";
        }
        else
        {
            /*echo "<h1>You have encountered a problem.</h1>";
            echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";*/
        }
    
    
    }
}

    else {
        //nothing
    }

//post the consistant variables

$hiddenSubmittedUser = htmlspecialchars($_POST['hiddenSubmittedUser']);
$recipeName = htmlspecialchars($_POST['recipeName']);
$recipeName = str_replace(" ","-",$recipeName);
$numberOfServings = htmlspecialchars($_POST['numberOfServings']);
$cookingTime = htmlspecialchars($_POST['cookingTime']);
$productImage = htmlspecialchars($_POST['productImage']);
$recipeInstructions = htmlspecialchars($_POST['recipeInstructions']);
$author = htmlspecialchars($_POST['author']);
$submission_date = date('Y-m-d');
$submission_time = date("H:i:s");


    //Create loop to finish adding the additional ingredients to the array
if (isset($addAnotherIngredientForSubmit)){
    if ($addAnotherIngredientForSubmit > 0 ) {

        global $newIngredients;
        global $addAnotherIngredientForSubmit;
        global $ingredientNameArray;
        global $measurementUnitsArray;
        global $measurementArray;

        for ($i = 0; $i < $addAnotherIngredientForSubmit; $i++) {
        
        $ingredientNumber = "ingredientSelection" . $i;
        $measurementUnitsNumber = "measurementUnits" . $i;
        $measurementNumber = "measurement" . $i;


        if( isset($_POST[$ingredientNumber])){
        
            $ingredientName = htmlspecialchars($_POST[$ingredientNumber]);
            $ingredientName = str_replace(" ","-",$ingredientName); //convert spaces to -

        }

        if( isset($_POST[$measurementUnitsNumber])){

        $measurementUnits = htmlspecialchars($_POST[$measurementUnitsNumber]);
        
        }

        if( isset($_POST[$measurementNumber])){

        $measurement = htmlspecialchars($_POST[$measurementNumber]);

        }

        $submission_date = date('Y-m-d');
        $submission_time = date("H:i:s");
    
        //Push the ingredients along with their measurements into arrays 
        array_push($ingredientNameArray,$ingredientName);
        array_push($measurementUnitsArray,$measurementUnits);
        array_push($measurementArray,$measurement);

    }
  }
}
    else {//nothing 

    }


        $ingredientNameArray = implode(",", $ingredientNameArray);
        $measurementUnitsArray = implode(",", $measurementUnitsArray);
        $measurementArray = implode(",", $measurementArray);



//SQL EXAMPLE #1
//Build the SQL Command to add the record  This technique is good for adding a lot of fields 
// or when you need to add and remove fields as your develop/test your application.


    $sql = "INSERT INTO recipeTable (";
    $sql .= "submission_date, ";
    $sql .= "submission_time, ";
    $sql .= "author, ";
    $sql .= "recipeName, ";
    $sql .= "numberOfServings, ";
    $sql .= "cookingTime, ";
    $sql .= "productImage, ";
    $sql .= "ingredientNameArray, ";  //This will be an array
    $sql .= "measurementUnitsArray, ";  //This will be an array
    $sql .= "measurementArray, "; //This will be an array
    $sql .= "recipeInstructions, ";
    $sql .= "submitted_By "; 

    //Last column in the list does NOT have a comma after it.

    $sql .= ") VALUES (";
    $sql .= "'$submission_date', ";
    $sql .= "'$submission_time', "; 
    $sql .= "'$author', ";
    $sql .= "'$recipeName', ";
    $sql .= "'$numberOfServings', ";
    $sql .= "'$cookingTime', ";
    $sql .= "'$productImage', ";
    $sql .= "'$ingredientNameArray', ";  //This will be an array
    $sql .= "'$measurementUnitsArray', ";  //This will be an array
    $sql .= "'$measurementArray', "; //This will be an array
    $sql .= "'$recipeInstructions', ";
    $sql .= "'$hiddenSubmittedUser' ";

    //Last column in the list does NOT have a comma after it.

    $sql .= ");";
    //Test the SQL command to see if it correctly formatted.
    //echo "<p>$sql</p>";
    


?>
</head>

<body>

<?php
//Run the SQL command using the database you connected with
if (mysqli_query($link,$sql) )
{
    echo "<div class=\"beigeBodyWrapper80\">";
    echo "<h2>Your recipe for " . $recipeName . " has been successfully added to the database.</h2>";
    echo "</div>";
    
    echo "<div class=\"beigeBodyWrapper80\">";
    echo "<p>Please <a href='recipeProject.php'>view</a> View Recipes.</p>";
    echo "</div>";
}
else
{
    /*echo "<h1>You have encountered a problem.</h1>";
    echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
    */
}

mysqli_close($link);    //closes the connection to the database once this page is complete.

?>
</body>
</html>