<?php 

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>

<?
    global $titleValue; 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Recipes Galore 
<?php 

if(isset($titleValue)){

    echo ": " . $titleValue;

    } 

else
    { 
        echo "!";
    } 
?> 
</title>
<?php include 'externalScripts.php'; ?>

<script src="recipeProjectFunctions.js"></script>

       <script type="text/javascript" charset="utf-8">
    //   $(function(){
        
    //     window.f = new flux.slider('#slider', {
    //       autoplay: false,
    //       pagination: false
    //     });
        
    //     // Setup a listener for user requested transitions
    //     $('div.transitions').bind('click', function(event){
    //       event.preventDefault();

    //       // If this is a 3D transform and the browser doesn't support 3D then inform the user

          
    //       window.f.next(event.target.href.split('#')[1]);
    //     });
    //   });
     </script>

<!--       echo "$(.thumbnail).animate({";
echo "transform: 'translateX(50px)'";
echo "});";
//echo "$(\".thumbnail\").css(\"-webkit-transform\",\"scale(1,1) rotateZ(360deg) rotateX(360deg)\");";
     -->
<style>

.newRecipe p {

    text-align: center;

}

</style>
</head>
<?php include 'recipeNavigationMenu.php'; ?>
<script>

<?php 

if(isset($activePage)){

echo "$('#" . $activePage ."').toggleClass(\"active\");"; 

}
?>

</script>

<div class="bodyWrapper">
