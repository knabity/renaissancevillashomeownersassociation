
<?php include 'externalScripts.php'; ?>

      <script type="text/javascript" charset="utf-8">
      $(function(){
        
        window.f = new flux.slider('#slider', {
          autoplay: false,
          pagination: false
        });
        
        // Setup a listener for user requested transitions
        $('div.transitions').bind('click', function(event){
          event.preventDefault();

          // If this is a 3D transform and the browser doesn't support 3D then inform the user

          
          window.f.next(event.target.href.split('#')[1]);
        });
      });
    </script>
  </head>
        <div id="slider">
          <img src="images/CrockpotChili.jpg" alt="" />
          <img src="images/KeyLimePie.jpg" alt="" />
          <img src="images/Hamburgers.jpg" alt="" />
          <img src="images/StuffedJalepenos.jpg" alt="" />
        </div>
        <div class="transitions">
          <h2>2D Transitions</h2>
          <ul id="trans2D">
            <li><a href="#bars">Bars</a></li>
            <li><a href="#zip">Zip</a></li>
            <li><a href="#blinds">Blinds</a></li>
            <li><a href="#blocks">Blocks</a></li>
            <li><a href="#concentric">Concentric</a></li>
            <li><a href="#warp">Warp</a></li>
            <li><a href="#slide">Slide</a></li>
          </ul>
          <h2>3D Transitions</h2>
          <ul id="trans3d">
            <li><a href="#bars3d">Bars3D</a></li>
            <li><a href="#cube">Cube</a></li>
            <li><a href="#tiles3d" class="new">Tiles3D</a></li>
            <li><a href="#blinds3d" class="new">Blinds3D</a></li>
            <li><a href="#turn" class="new">Turn</a></li>
          </ul>
      </div>

  </body>
</html>