<?php include 'loginRequired.php'; ?>
<?php $titleValue = "Update User"; ?>
<?php include 'homeOwnersAssociationHeader.php'; ?>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<?php

$updateRecId = $_GET['recordId'];		//This comes from the get request generated in the Select Events page link.  Much like the delete works

$sql = "SELECT * FROM homeOwnersAssociationOwnersTable WHERE homeOwnersAssociationOwner_id = $updateRecId";	//Finds a specific record in the table

$userID = $updateRecId;
	//echo "<p>The SQL Command: $sql </p>"; 	//For testing purposes as needed.

//Run the SQL command against the database
$result = mysqli_query($link,$sql);
//Check the result to make sure it ran correctly
if (!$result)
{
	echo "<h1>You have encountered a problem with your update.</h1>";
	die( "<h2>" . mysqli_error($link) . "</h2>") ;		//This will display the error and then stop the page
}
$row = mysqli_fetch_array($result);		//Turn the result into an associative array so we can get the column values
?>
<?php
if($_SESSION['userID'] != $row['homeOwnersAssociationOwner_id']){
                //check if it was not written by the administrator.
                if($_SESSION['adminPrivileges'] != "1"){
                //echo you don't have permissions to delete it.
                echo "<div class=\"beigeBodyWrapper80\">";
                echo "<h1 class=\"red\">You do not have permissions to update this user.</h1>";
                echo "</div>";
                }

                else{
                     
                     ?>
                     
                     <div class="beigeBodyWrapper80">
                     <h1>Update User Form</h1>
                     <form id="userForm" name="userForm" method="post" action="updateUsers.php">
                     
                     <input type="hidden" class="hidden" name="userID" value= "<?php echo $updateRecId; ?>">
                     
                       <p>First Name:  
                         <input type="text" name="firstName" id="firstName" 
                           value="<?php echo $row['firstName']; ?>" required />
                       </p>
                         <p>Last Name:  
                         <input type="text" name="lastName" id="lastName" 
                           value="<?php echo $row['lastName']; ?>" required />
                       </p>
                       <p>User Name:  
                         <input type="text" name="userName" id="userName" 
    	                     value="<?php echo $row['userName']; ?>" required />
                       </p>
                       <p>Email:  
                         <input type="emailAddress" name="emailAddress" id="emailAddress" value="<?php echo $row['emailAddress']; ?>" required />
                       </p>
                       <p>Phone Number:  
                         <input type="text" name="phoneNumber" id="phoneNumber" value="<?php echo $row['phoneNumber']; ?>" required />
                       </p> 
                       <p>Do you want your phone number listed? </p> 
                         <label>
                         <?php if($row['listPhone'] == "yes"){ ?>
                     
                         <input type="radio" name="listPhone" value="yes" checked = "checked">
                         Yes</label>
                         <label>
                         <input type="radio" name="listPhone" value="no">
                         No</label>
                     
                         <?php
                         }
                         else{
                     
                         ?>
                     
                         <input type="radio" name="listPhone" value="yes" >
                         Yes</label>
                         <label>
                         <input type="radio" name="listPhone" value="no" checked = "checked">
                         No</label>
                     
                         <?php
                     
                         }
                     
                         ?>
                     
                         <p>User Password:  
                         <input type="password" name="password" id="password" 
       	                     value="<?php echo $row['password']; ?>" />
                       </p>
                       
  	                     <!--The hidden form contains the record if for this record. 
    	                     You can use this hidden field to pass the value of record id 
                             to the update page.  It will go as one of the name value
                             pairs from the form.
                         -->
                       <input type="hidden" name="user_id" id="user_id"
                           value="<?php echo $updateRecId; ?>" /> 
                       
                       <p>
                         <input type="submit" name="button" id="button" value="Update" />
                         <input type="reset" name="button2" id="button2" value="Clear Form" />
                       </p>
                     </form>

                     <?php
                     
                     }
                     }
                     else {

                     ?>

                     <div class="beigeBodyWrapper80">
                     <h1>Update User Form</h1>
                     <form id="userForm" name="userForm" method="post" action="updateUsers.php">
                     
                     <input type="hidden" class="hidden" name="userID" value= "<?php echo $updateRecId; ?>">
                     
                       <p>First Name:  
                         <input type="text" name="firstName" id="firstName" 
                           value="<?php echo $row['firstName']; ?>" required />
                       </p>
                         <p>Last Name:  
                         <input type="text" name="lastName" id="lastName" 
                           value="<?php echo $row['lastName']; ?>" required />
                       </p>
                       <p>User Name:  
                         <input type="text" name="userName" id="userName" 
                           value="<?php echo $row['userName']; ?>" required />
                       </p>
                       <p>Email:  
                         <input type="emailAddress" name="emailAddress" id="emailAddress" value="<?php echo $row['emailAddress']; ?>" required />
                       </p>
                       <p>Phone Number:  
                         <input type="text" name="phoneNumber" id="phoneNumber" value="<?php echo $row['phoneNumber']; ?>" required />
                       </p> 
                       <p>Do you want your phone number listed? </p> 
                         <label>
                         <?php if($row['listPhone'] == "yes"){ ?>
                     
                         <input type="radio" name="listPhone" value="yes" checked = "checked">
                         Yes</label>
                         <label>
                         <input type="radio" name="listPhone" value="no">
                         No</label>
                     
                         <?php
                         }
                         else{
                     
                         ?>
                     
                         <input type="radio" name="listPhone" value="yes" >
                         Yes</label>
                         <label>
                         <input type="radio" name="listPhone" value="no" checked = "checked">
                         No</label>
                     
                         <?php
                     
                         }
                     
                         ?>
                     
                         <p>User Password:  
                         <input type="password" name="password" id="password" 
                             value="<?php echo $row['password']; ?>" />
                       </p>
                       
                         <!--The hidden form contains the record if for this record. 
                           You can use this hidden field to pass the value of record id 
                             to the update page.  It will go as one of the name value
                             pairs from the form.
                         -->
                       <input type="hidden" name="user_id" id="user_id"
                           value="<?php echo $updateRecId; ?>" /> 
                       
                       <p>
                         <input type="submit" name="button" id="button" value="Update" />
                         <input type="reset" name="button2" id="button2" value="Clear Form" />
                       </p>
                     </form>
                     <?php 

                   }

                   ?>
</div>
<?php include 'homeOwnersAssociationFooter.php'; ?>
</body>
</html>
