<?php include 'loginRequired.php'; ?>
<?php $titleValue = "Update User"; ?>
<?php include 'homeOwnersAssociationHeader.php'; ?>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<?php

include 'dbConnect.php';	//connects to the database

//Get the name value pairs from the $_POST variable into PHP variables
//For this example I am using PHP variables with the same name as the name atribute from the HTML form

$firstName = htmlspecialchars($_POST['firstName']);
$lastName = htmlspecialchars($_POST['lastName']);
$userName = htmlspecialchars($_POST['userName']);
$emailAddress = htmlspecialchars($_POST['emailAddress']);
$phoneNumber = htmlspecialchars($_POST['phoneNumber']);
$listPhone = htmlspecialchars($_POST['listPhone']);
$password = htmlspecialchars($_POST['password']);
$userID = htmlspecialchars($_POST['userID']);

$submission_date = date('Y-m-d');
$submission_time = date("H:i:s");


?>

<div class="beigeBodyWrapper80">
<h2>Update Users</h2>
</div>
<div class="beigeBodyWrapper80">

<?php
//Create the SQL UPDATE query or command  
	$sql = "UPDATE homeOwnersAssociationOwnersTable SET " ;
	$sql .= "firstName='$firstName', ";
	$sql .= "lastName='$lastName', ";
	$sql .= "userName='$userName', ";
	$sql .= "emailAddress='$emailAddress', ";
	$sql .= "phoneNumber='$phoneNumber', ";
	$sql .= "listPhone='$listPhone', ";
	$sql .= "password='$password', ";
	$sql .= "submission_date='$submission_date', ";		
	$sql .= "submission_time='$submission_time' ";		//NOTE last one does NOT have a comma after it
	$sql .= " WHERE (homeOwnersAssociationOwner_id='$userID')";		//VERY IMPORTANT  Only delete the requested record id
	
	//echo "<h3>$sql</h3>";			//testing

if (mysqli_query($link,$sql) )
{
	echo "<h1>Your record has been successfully UPDATED.</h1>";
	echo "<p><a href='owners.php'>Return to Owners Page</a></p>";
}
else
{
	echo "<h1>You have encountered a problem.</h1>";
	echo "<h2 style='color:red'>" . mysqli_error($link) . "</h2>";
}
mysqli_close($link);	//closes the connection to the database once this page is complete.
?>
</div>
<?php include 'homeOwnersAssociationFooter.php'; ?>
