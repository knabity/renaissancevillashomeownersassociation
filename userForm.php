<?php include 'loginRequired.php'; ?>
<?php $titleValue = "Add New User"; ?>
<?php $activePage = "owners"; ?>
<?php include 'homeOwnersAssociationHeader.php'; ?>
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<?php 

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
?>
<?php 

include 'dbConnect.php';
?>
<head>
<style>
/*p {

    text-align: left;

}

input {

    vertical-align: right;

}*/
</style>
</head>
<body>
<div class="beigeBodyWrapper80">
<h1>Add New User</h1>
</div>
<div class="beigeBodyWrapper80">
<form id="eventsForm" name="eventsForm" method="post" action="insertUser.php">
<div class="leftClass">
<table>
<tr>
      <td><label for="firstName">First Name: </label> </td>
      <td><input type="text" name="firstName" id="firstName" required /><br /></td>

      <td><label for="lastName">Last Name: </label>  </td>
      <td><input type="text" name="lastName" id="lastName" required /><br /></td>
</tr>
<tr>
  <!-- Create Hidden Field for City State & Zip Code since they are consistent -->
  <input type="hidden" name="cityStateZip" value="Ankeny, Iowa 50021">
</tr>
<tr>
      <td><label for="streetAddress">Street Address: </label>  </td>
      <td><input type="text" name="streetAddress" id="streetAddress" required /><br /></td>

      <td><label for="userName">User Name: </label> </td>
      <td><input type="text" name="userName" id="userName" required /><br /></td>
</tr>
<tr>
      <td><label for="email">Email: </label> </td>
      <td><input type="email" name="emailAddress" id="emailAddress" required /><br /></td>

      <td><label for="phoneNumber">Phone Number: </label> </td>
      <td><input type="text" name="phoneNumber" id="phoneNumber" required /><br /></td>
</tr>
</table>
</div>

  <p>Do you want your phone number listed? </p> 
    <label>
    <input type="radio" name="listPhone" value="yes">
    Yes</label>
    <label>
    <input type="radio" name="listPhone" value="no">
    No</label>

<?php 
              if(isset($_SESSION['adminPrivileges'])){
                if($_SESSION['adminPrivileges'] == "1"){
                //if the administrator is adding the user give them the opportunity to grand admin privileges.
?>
<p>Do you want to grant this user administrative rights?</p>
    
    <label>
    <input type="radio" name="adminPrivileges" value="1">
    Yes</label>
    <label>
    <input type="radio" name="adminPrivileges" value="0">
    No</label>

<?php 
                }
                else {

                }
              }//end of checking for admin privileges

              else {}
?>
  <p>Password:  
    <input type="password" name="password" id="password" />
  </p>

  <p>
    <input type="submit" name="button" id="button" value="Add User" />
    <input type="reset" name="button2" id="button2" value="Clear Form" />
  </p>
</form>
</div>

<?php include 'homeOwnersAssociationFooter.php'; ?>
